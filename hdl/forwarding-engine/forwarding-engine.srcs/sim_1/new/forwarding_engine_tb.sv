`timescale 1ns / 1ps

import forwarding_engine_pkg::*;

module forwarding_engine_tb();

logic clk;
logic rst;

logic                         htab_wea [`HTAB_SUBTABLES_NUM-1:0] = '{default:'0};
logic  [`HTAB_ADDR_WIDTH-1:0] htab_addra [`HTAB_SUBTABLES_NUM-1:0] = '{default:'0};
logic  [`HTAB_DATA_WIDTH-1:0] htab_dina [`HTAB_SUBTABLES_NUM-1:0] = '{default:'0};

class driver #(int TDATA_WIDTH = 0);
virtual axi4stream_if #(.TDATA_WIDTH(TDATA_WIDTH)) vif;

automatic task drive_axis(input string path, input int pid);
    string s;
    t_forwarding_engine_request req;
    
    int f = $fopen(path, "r");
    if (!f)
        $error("File could not be open: ", path);
    else begin
        while($fgets(s,f)) begin
            $sscanf(s,"%h\n", req);
            @(vif.cb) begin
                vif.tvalid = '1;
                vif.tdata = req;
                `ifdef DEBUG
                $display("Request: '{pid:%2d,id:%4d,smac:0x%h,dmac:0x%h,vid:%2d,has_vid:%1d,prio:%1d,has_prio:%1d}, time:%t", 
                                     pid, req.id, req.smac, req.dmac, req.vid, req.has_vid, req.prio, req.has_prio, $time);
                `endif
            end
            
            for (integer i=0; i<`PORTS_NUM_PER_ENGINE-1; i++) begin
                @(vif.cb) begin
                    vif.tvalid = '0;
                end
            end
        end
        $fclose(f);
    end
endtask

automatic task reset_axis();
    @(vif.cb) begin
        vif.tvalid = '0;
        vif.tdata = '0;
    end
endtask

endclass

class monitor #(int TDATA_WIDTH = 0);
virtual axi4stream_if #(.TDATA_WIDTH(TDATA_WIDTH)) vif;

automatic task set_axis_tready();
    @(vif.cb)
       vif.tready = '1;
endtask


automatic task monitor_axis(input string path, input int pid);
    string s;
    t_forwarding_engine_response resp;
    t_forwarding_engine_response rcvd_resp;
    
    int f = $fopen(path, "r");
    if (!f)
        $error("File could not be open: ", path);
    else begin
        while($fgets(s,f)) begin
            $sscanf(s,"%h\n", resp);
            forever begin
                @(vif.cb)
                    if (vif.tvalid && vif.tready) begin
                        `ifdef DEBUG
                        rcvd_resp = vif.tdata;
                        $display("Received response: '{pid:%2d,id:%4d,port_mask:%24b,prio:%1d,drop:%1d}, time:%t", 
                                                       pid, rcvd_resp.id, rcvd_resp.port_mask, rcvd_resp.prio, rcvd_resp.drop, $time);
                        `endif
                        if (vif.tdata != resp) begin
                            `ifdef DEBUG
                            $display("Expected response: '{pid:%2d,id:%4d,port_mask:%24b,prio:%1d,drop:%1d}, time:%t", 
                                                           pid, resp.id, resp.port_mask, resp.prio, resp.drop, $time);
                            `endif
                            $error("Response ERROR! time: %t", $time);
                            $finish;
                        end
                        break;
                    end
            end
        end
        $fclose(f);
    end
endtask

endclass

task reset();
    mon_m_axis_tru.set_axis_tready();
    mon_m_axis_learning_fifo.set_axis_tready();
    drv_s_axis_tru.reset_axis();
    
    for (int i=0; i<`PORTS_NUM_PER_ENGINE; i++) begin
        automatic int j = i;
        fork
            begin
                mon_m_axis[j].set_axis_tready();
                drv_s_axis[j].reset_axis();
            end
        join_none
    end
    wait fork;
endtask

task run();
    for (int i=0; i<`PORTS_NUM_PER_ENGINE; i++) begin
        automatic int j = i;
        fork
            drv_s_axis[j].drive_axis($sformatf("input%0d.stim",j), j);
            mon_m_axis[j].monitor_axis($sformatf("output%0d.stim",j), j);
        join_none
    end
    wait fork;
endtask

axi4stream_if #(.TDATA_WIDTH($size(t_forwarding_engine_request))) s_axis [`PORTS_NUM_PER_ENGINE-1:0] (clk, rst);
axi4stream_if #(.TDATA_WIDTH($size(t_forwarding_engine_response))) m_axis [`PORTS_NUM_PER_ENGINE-1:0] (clk, rst);
axi4stream_if #(.TDATA_WIDTH($size(t_tru_response))) s_axis_tru (clk, rst);
axi4stream_if #(.TDATA_WIDTH($size(t_tru_request))) m_axis_tru (clk, rst);
axi4stream_if #(.TDATA_WIDTH($size(t_learning_fifo_entry))) m_axis_learning_fifo (clk, rst);

driver #(.TDATA_WIDTH($size(t_forwarding_engine_request))) drv_s_axis[`PORTS_NUM_PER_ENGINE-1:0];
monitor #(.TDATA_WIDTH($size(t_forwarding_engine_response))) mon_m_axis[`PORTS_NUM_PER_ENGINE-1:0];
driver #(.TDATA_WIDTH($size(t_tru_response))) drv_s_axis_tru;
monitor #(.TDATA_WIDTH($size(t_tru_request))) mon_m_axis_tru;
monitor #(.TDATA_WIDTH($size(t_learning_fifo_entry))) mon_m_axis_learning_fifo;

forwarding_engine dut (
    .Clk(clk),
    .Rst(rst),
    .En('1),
    .Tru_en('0),
    .Ageing_bitmap_ptr('1),
    .Pass_all({{12{1'b1}}, 1'b0, {11{1'b1}}}),
    .Pass_link_local({{11{1'b1}}, 1'b0, {12{1'b1}}}),
    .B_unrec({`PORTS_NUM{1'b1}}),
    .Learn_en({`PORTS_NUM{1'b1}}),
    .S_axis(s_axis),
    .M_axis(m_axis),
    .S_axis_tru(s_axis_tru),
    .M_axis_tru(m_axis_tru),
    .M_axis_learning_fifo(m_axis_learning_fifo),
    .Vtab_wea('0),
    .Vtab_addra('0),
    .Vtab_dina('0),
    .Htab_src_wea(htab_wea),
    .Htab_src_addra(htab_addra),
    .Htab_src_dina(htab_dina),
    .Htab_dst_wea(htab_wea),
    .Htab_dst_addra(htab_addra),
    .Htab_dst_dina(htab_dina),
    .Mtab_src_wea('0),
    .Mtab_src_addra('0),
    .Mtab_src_dina('0),
    .Mtab_dst_wea('0),
    .Mtab_dst_addra('0),
    .Mtab_dst_dina('0),
    .Ageing_bitmap_wea('0),
    .Ageing_bitmap_addra('0),
    .Ageing_bitmap_dina('0),
    .Ageing_bitmap_douta(),
    .dbg_mtab_unmatched(),
    .dbg_learning_fifo_almost_full(),
    .dbg_axis_learning_fifo_tready(),
    .dbg_axis_decision_maker_tready()
);

initial begin
    drv_s_axis_tru = new();
    drv_s_axis_tru.vif = s_axis_tru;
    mon_m_axis_tru = new();
    mon_m_axis_tru.vif = m_axis_tru;
    mon_m_axis_learning_fifo = new();
    mon_m_axis_learning_fifo.vif = m_axis_learning_fifo;
end

generate
for (genvar i=0; i<`PORTS_NUM_PER_ENGINE; i++)
    initial begin
        drv_s_axis[i] = new();
        drv_s_axis[i].vif = s_axis[i];
        mon_m_axis[i] = new();
        mon_m_axis[i].vif = m_axis[i];
    end
endgenerate

initial begin
    
    clk = 0;
    rst = 1;
    
    reset();
    
    #(`CLK_PERIOD*16); rst = 0;
    #(`CLK_PERIOD);
    
    run();
    
    $display("Simulation finished with SUCCESS.");
    $finish;

end

initial begin
    `ifdef DEBUG
        $display("Forwarding engine request size [b]: %d", $size(t_forwarding_engine_request));
        $display("Forwarding engine response size [b]: %d", $size(t_forwarding_engine_response));
        $display("TRU module request size [b]: %d", $size(t_tru_request));
        $display("TRU module response size [b]: %d", $size(t_tru_response));
        $display("Learning FIFO entry size [b]: %d", $size(t_learning_fifo_entry));
        $display("VLAN table entry size [b]: %d", $size(t_vtab_entry));
        $display("HASH table entry size [b]: %d", $size(t_htab_entry));
        $display("MAC table entry size [b]: %d", $size(t_mtab_entry));
        $display("HASH lookup info size [b]: %d", $size(t_htab_info));
        $display("Forwarding engine request decision info size [b]: %d", $size(t_forwarding_engine_request_decision_info));
        $display("VLAN table decision info size [b]: %d", $size(t_vtab_decision_info));
        $display("MAC table decision info size [b]: %d", $size(t_mtab_decision_info));
        $display(`PORT_ID_WIDTH);
    `endif
end

always #(`CLK_PERIOD/2) clk <= ~clk;


endmodule
