//-----------------------------------------------------------------------------
// Title      : VLAN table reader
// Project    : White Rabbit Switch
//-----------------------------------------------------------------------------
// File       : vtab_reader.sv
// Authors    : Magdalena Zarychta
// Company    : CERN BE-Co-HT
// Created    : 2021-09-15
// Last update: 2021-09-15
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//-----------------------------------------------------------------------------
// Description:
//
// The VTAB reader module reads a VLAN table entry corresponding to the VLAN ID 
// from the request. Its parameter is a VLAN table read latency given in clock
// cycles. On the basis of the forwarding engine request and the read VLAN 
// table entry the module forwards:
// • part of the request data set needed to make final forwarding decision 
//   (M_axis_req),
// • part of the VLAN table entry needed to make final forwarding decision 
//   (M_axis_vtab),
// • Filtering ID and source MAC address for HASH table lookup 
//   (M_axis_htab_src),
// • Filtering ID and destination MAC address for HASH table lookup 
//   (M_axis_htab_dst),
// • learning FIFO data set (M_axis_learning_fifo),
// • TRU module request (M_axis_tru)
// 
//-----------------------------------------------------------------------------
//
// Copyright (c) 2012 CERN / BE-CO-HT
//
// This source file is free software; you can redistribute it   
// and/or modify it under the terms of the GNU Lesser General   
// Public License as published by the Free Software Foundation; 
// either version 2.1 of the License, or (at your option) any   
// later version.                                               
//
// This source is distributed in the hope that it will be       
// useful, but WITHOUT ANY WARRANTY; without even the implied   
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
// PURPOSE.  See the GNU Lesser General Public License for more 
// details.                                                     
//
// You should have received a copy of the GNU Lesser General    
// Public License along with this source; if not, download it   
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author    Description
// 2021-09-15  1.0      mzarychta created
//-----------------------------------------------------------------------------

`timescale 1ns / 1ps

import forwarding_engine_pkg::*;

module vtab_reader #(
    parameter [1:0] VTAB_READ_LATENCY = 1 // available values: 1/2
)(

    input Clk,
    input Rst,
    
    input                                                 S_axis_tvalid,
    input                     t_forwarding_engine_request S_axis_tdata,
    input                            [`PORT_ID_WIDTH-1:0] S_axis_tdest,
    
    output                                                M_axis_req_tvalid,
    output      t_forwarding_engine_request_decision_info M_axis_req_tdata,
    
    output                                                M_axis_vtab_tvalid,
    output                           t_vtab_decision_info M_axis_vtab_tdata,
    
    output                                                M_axis_htab_src_tvalid,
    output                                    t_htab_info M_axis_htab_src_tdata,
    
    output                                                M_axis_htab_dst_tvalid,
    output                                    t_htab_info M_axis_htab_dst_tdata,
    
    output                                                M_axis_learning_fifo_tvalid,
    output                          t_learning_fifo_entry M_axis_learning_fifo_tdata,
    
    output                                                M_axis_tru_tvalid,
    output                                  t_tru_request M_axis_tru_tdata,
    
    output                         [`VTAB_ADDR_WIDTH-1:0] Vtab_addr,
    input                                    t_vtab_entry Vtab_dout

);

t_forwarding_engine_request    axis_tdata_reg = 0;
t_forwarding_engine_request    axis_tdata_reg_q = 0;
reg       [`PORT_ID_WIDTH-1:0] axis_tdest_reg = 0;
reg       [`PORT_ID_WIDTH-1:0] axis_tdest_reg_q = 0;
reg                            axis_tvalid_reg = 0;
reg                            axis_tvalid_reg_q = 0;

assign Vtab_addr = S_axis_tdata.has_vid ? S_axis_tdata.vid : '0;

generate
if (VTAB_READ_LATENCY == 1) begin
    always @ (posedge Clk)
        if (Rst)
            axis_tvalid_reg <= 0;
        else
            begin
                axis_tvalid_reg <= S_axis_tvalid;
                axis_tdata_reg <= S_axis_tdata;
                axis_tdest_reg <= S_axis_tdest;
            end
end
else if (VTAB_READ_LATENCY == 2) begin
    always @ (posedge Clk)
        if (Rst)
            axis_tvalid_reg <= 0;
        else
        begin
            axis_tvalid_reg_q <= S_axis_tvalid;
            axis_tdata_reg_q <= S_axis_tdata;
            axis_tdest_reg_q <= S_axis_tdest;
            
            axis_tvalid_reg <= axis_tvalid_reg_q;
            axis_tdata_reg <= axis_tdata_reg_q;
            axis_tdest_reg <= axis_tdest_reg_q;
        end 
end
endgenerate
  
assign M_axis_req_tvalid           = axis_tvalid_reg;
assign M_axis_vtab_tvalid          = axis_tvalid_reg;
assign M_axis_htab_src_tvalid      = axis_tvalid_reg;
assign M_axis_htab_dst_tvalid      = axis_tvalid_reg;
assign M_axis_learning_fifo_tvalid = axis_tvalid_reg;
assign M_axis_tru_tvalid           = axis_tvalid_reg;

assign M_axis_req_tdata.id       = axis_tdata_reg.id; 
assign M_axis_req_tdata.pid      = axis_tdest_reg; 
assign M_axis_req_tdata.prio     = axis_tdata_reg.prio; 
assign M_axis_req_tdata.has_prio = axis_tdata_reg.has_prio;
assign M_axis_req_tdata.rsvd     = '0; // for future use
    
assign M_axis_vtab_tdata.drop          = Vtab_dout.drop;
assign M_axis_vtab_tdata.has_prio      = Vtab_dout.has_prio;
assign M_axis_vtab_tdata.prio_override = Vtab_dout.prio_override;
assign M_axis_vtab_tdata.prio          = Vtab_dout.prio;
assign M_axis_vtab_tdata.port_mask     = Vtab_dout.port_mask[`PORTS_NUM-1:0];
assign M_axis_vtab_tdata.rsvd          = '0; // for future use

assign M_axis_htab_src_tdata.fid = Vtab_dout.fid;
assign M_axis_htab_src_tdata.mac = axis_tdata_reg.smac;

assign M_axis_htab_dst_tdata.fid = Vtab_dout.fid;
assign M_axis_htab_dst_tdata.mac = axis_tdata_reg.dmac;

assign M_axis_learning_fifo_tdata.pid      = axis_tdest_reg; 
assign M_axis_learning_fifo_tdata.smac     = axis_tdata_reg.smac;
assign M_axis_learning_fifo_tdata.has_vid  = axis_tdata_reg.has_vid;
assign M_axis_learning_fifo_tdata.vid      = axis_tdata_reg.vid;
assign M_axis_learning_fifo_tdata.has_prio = axis_tdata_reg.has_prio;
assign M_axis_learning_fifo_tdata.prio     = axis_tdata_reg.prio;
assign M_axis_learning_fifo_tdata.rsvd     = '0; // for future use

assign M_axis_tru_tdata.smac    = axis_tdata_reg.smac; 
assign M_axis_tru_tdata.dmac    = axis_tdata_reg.dmac; 
assign M_axis_tru_tdata.prio    = axis_tdata_reg.prio;
assign M_axis_tru_tdata.fid     = Vtab_dout.fid; 
assign M_axis_tru_tdata.reqMask = pid_to_port_mask(axis_tdest_reg);
assign M_axis_tru_tdata.isHP    = 0; // for future use
assign M_axis_tru_tdata.isBR    = 0; // for future use
assign M_axis_tru_tdata.rsvd    = '0; // for future use

endmodule
