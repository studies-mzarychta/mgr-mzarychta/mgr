//-----------------------------------------------------------------------------
// Title      : HASH table lookup engine
// Project    : White Rabbit Switch
//-----------------------------------------------------------------------------
// File       : htab_lookup.sv
// Authors    : Magdalena Zarychta
// Company    : CERN BE-Co-HT
// Created    : 2021-09-15
// Last update: 2021-09-15
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//-----------------------------------------------------------------------------
// Description:
//
// The HTAB Lookup module is responsible for performing HASH table lookup for 
// a given Filtering ID and MAC address. Its parameter is a HASH table read 
// latency given in clock cycles. The module forwards a found HASH table entry
// or sets a not found flag on an output signal tuser.
// 
//-----------------------------------------------------------------------------
//
// Copyright (c) 2012 CERN / BE-CO-HT
//
// This source file is free software; you can redistribute it   
// and/or modify it under the terms of the GNU Lesser General   
// Public License as published by the Free Software Foundation; 
// either version 2.1 of the License, or (at your option) any   
// later version.                                               
//
// This source is distributed in the hope that it will be       
// useful, but WITHOUT ANY WARRANTY; without even the implied   
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
// PURPOSE.  See the GNU Lesser General Public License for more 
// details.                                                     
//
// You should have received a copy of the GNU Lesser General    
// Public License along with this source; if not, download it   
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author    Description
// 2021-09-15  1.0      mzarychta created
//-----------------------------------------------------------------------------

`timescale 1ns / 1ps

import forwarding_engine_pkg::*;

module htab_lookup  #(
    parameter [1:0] HTAB_READ_LATENCY = 1 // available values: 1/2
)(

    input Clk,
    input Rst,
    
    input                                S_axis_tvalid,
    input                    t_htab_info S_axis_tdata,
    
    output reg                           M_axis_tvalid,
    output                  t_htab_entry M_axis_tdata,
    output reg                           M_axis_tuser,
    
    output        [`HTAB_ADDR_WIDTH-1:0] Htab_addr [`HTAB_SUBTABLES_NUM-1:0],
    input                   t_htab_entry Htab_dout [`HTAB_SUBTABLES_NUM-1:0]

);


reg axis_tvalid_reg = 0;
reg axis_tvalid_reg_q = 0;

t_htab_info axis_tdata_reg = 0;
t_htab_info axis_tdata_reg_q = 0;
wire [`SELECTOR_WIDTH-1:0] selector;

assign selector = {S_axis_tdata.fid, S_axis_tdata.mac};

// 0: CRC16_CCITT, CRC-16/KERMIT width=16 poly=0x1021 init=0x0000 refin=true refout=true xorout=0x0000 check=0x2189 residue=0x0000
// 1: CRC-16/CDMA2000 width=16 poly=0xc867 init=0xffff refin=false refout=false xorout=0x0000 check=0x4c06 residue=0x0000
// 2: CRC-16/ARC width=16 poly=0x8005 init=0x0000 refin=true refout=true xorout=0x0000 check=0xbb3d residue=0x0000 
// 3: CRC-32 width=32 poly=0x04c11db7 init=0xffffffff refin=true refout=true xorout=0xffffffff check=0xcbf43926 residue=0xdebb20e3
// 4: CRC-32C width=32 poly=0x1edc6f41 init=0xffffffff refin=true refout=true xorout=0xffffffff check=0xe3069283 residue=0xb798b438 

// calculate htab subtables addresses to be searched
generate
begin
for (genvar i = 0; i < `HTAB_SUBTABLES_NUM; i++)
    if (i == 0)
        assign Htab_addr[i] = reverse16b(nextCRC16_CCITT_D56(reverse_bits_in_bytes_D56(selector), 16'h0000));
    else if (i == 1)
        assign Htab_addr[i] = nextCRC16_CDMA2000_D56(selector, 16'hFFFF);
    else if (i == 2)
        assign Htab_addr[i] = reverse16b(nextCRC16_IBM_D56(reverse_bits_in_bytes_D56(selector), 16'h0000));
    else if (i == 3)
        assign Htab_addr[i] = reverse32b(nextCRC32_D56(reverse_bits_in_bytes_D56(selector), 32'hFFFFFFFF)) ^ 32'hFFFFFFFF;
    else if (i == 4)
        assign Htab_addr[i] = reverse32b(nextCRC32_C_D56(reverse_bits_in_bytes_D56(selector), 32'hFFFFFFFF)) ^ 32'hFFFFFFFF;
end
endgenerate

generate
if (HTAB_READ_LATENCY == 1) begin
    always @ (posedge Clk)
        if (Rst)
            axis_tvalid_reg <= 0;
        else
            begin
                axis_tvalid_reg <= S_axis_tvalid;
                axis_tdata_reg <= S_axis_tdata;
            end
end
else if (HTAB_READ_LATENCY == 2) begin
    always @ (posedge Clk)
        if (Rst)
            axis_tvalid_reg <= 0;
        else
            begin
                axis_tvalid_reg_q <= S_axis_tvalid;
                axis_tdata_reg_q <= S_axis_tdata;
                
                axis_tvalid_reg <= axis_tvalid_reg_q;
                axis_tdata_reg <= axis_tdata_reg_q;
            end 
end
endgenerate

assign M_axis_tvalid = axis_tvalid_reg;  
              
always @ *
    begin                    
        M_axis_tuser = 0; // on default cleared found flag
        M_axis_tdata = 0;
        
        for (int i = 0; i < `HTAB_SUBTABLES_NUM; i++)
            if (Htab_dout[i].fid == axis_tdata_reg.fid)
                if (Htab_dout[i].mac == axis_tdata_reg.mac)
                    begin
                        M_axis_tuser = 1; // set found flag
                        M_axis_tdata = Htab_dout[i]; // forward matching htab entry
                    end
    end
    
endmodule