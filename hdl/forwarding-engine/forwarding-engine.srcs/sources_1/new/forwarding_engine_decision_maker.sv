//-----------------------------------------------------------------------------
// Title      : Forwarding Engine Decision Maker
// Project    : White Rabbit Switch
//-----------------------------------------------------------------------------
// File       : forwarding_engine_decision_maker.sv
// Authors    : Magdalena Zarychta
// Company    : CERN BE-Co-HT
// Created    : 2021-09-15
// Last update: 2021-09-15
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//-----------------------------------------------------------------------------
// Description:
//
// The Forwarding Decision Maker module is responsible for making a final 
// forwarding engine decision on the basis of:
// • forwarding engine configuration,
// • forwarding engine request data set (S_axis_req),
// • VLAN table entry (S_axis_vtab),
// • MAC table entry for source MAC address or not found flag (S_axis_mtab_src),
// • MAC table entry for destination MAC address or not found flag 
//   (S_axis_mtab_dst),
// • TRU module response, if TRU processing enabled (S_axis_tru).
// It is assumed that all enumerated data sets are received in the same clock 
// cycle. The module implements the forwarding decision algorithm and determines 
// forwarding engine response components: drop flag, output port mask and frame 
// priority. Besides, if a HASH table entry for a source MAC address has not 
// been found and a learning process is enabled, it forwards a learning FIFO 
// data set (received on S_axis_learning_fifo interface) to a learning FIFO.
// 
//-----------------------------------------------------------------------------
//
// Copyright (c) 2012 CERN / BE-CO-HT
//
// This source file is free software; you can redistribute it   
// and/or modify it under the terms of the GNU Lesser General   
// Public License as published by the Free Software Foundation; 
// either version 2.1 of the License, or (at your option) any   
// later version.                                               
//
// This source is distributed in the hope that it will be       
// useful, but WITHOUT ANY WARRANTY; without even the implied   
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
// PURPOSE.  See the GNU Lesser General Public License for more 
// details.                                                     
//
// You should have received a copy of the GNU Lesser General    
// Public License along with this source; if not, download it   
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author    Description
// 2021-09-15  1.0      mzarychta created
//-----------------------------------------------------------------------------

`timescale 1ns / 1ps

import forwarding_engine_pkg::*;

module forwarding_engine_decision_maker(

    input Clk,
    input Rst,
    
    // global configuration
    input En,                         // forwarding processing enabled
    input Tru_en,                     // TRU processing enabled

    // per port configuration
    input [`PORTS_NUM-1:0] Pass_all ,         // pass frames received on given port    
    input [`PORTS_NUM-1:0] Pass_link_local,   // pass frames with link-local address received on a given port
    input [`PORTS_NUM-1:0] B_unrec,           // if 1, broadcast unrecognized frames received on a given port; 
                                              // if 0, drop unrecognized frames received on a given port
    input [`PORTS_NUM-1:0] Learn_en,          // if 1, learning enable on a given port
                                          
    input                                                 S_axis_req_tvalid,
    input       t_forwarding_engine_request_decision_info S_axis_req_tdata,
    
    input                                                 S_axis_vtab_tvalid,
    input                            t_vtab_decision_info S_axis_vtab_tdata,
    
    input                                                 S_axis_mtab_src_tvalid,
    input                            t_mtab_decision_info S_axis_mtab_src_tdata,
    
    input                                                 S_axis_mtab_dst_tvalid,
    input                            t_mtab_decision_info S_axis_mtab_dst_tdata,
    
    input                                                 S_axis_learning_fifo_tvalid,
    input                           t_learning_fifo_entry S_axis_learning_fifo_tdata,
    
    input                                                 S_axis_tru_tvalid,
    input                                  t_tru_response S_axis_tru_tdata,
    
    output reg                                            M_axis_tvalid,
    output                   t_forwarding_engine_response M_axis_tdata,
    output reg                       [`PORT_ID_WIDTH-1:0] M_axis_tdest,
    
    output reg                                            M_axis_learning_fifo_tvalid,
    output                          t_learning_fifo_entry M_axis_learning_fifo_tdata,
    
    output reg                                            dbg_mtab_unmatched
);

reg       [`PORTS_NUM-1:0] rsp_port_mask_reg = 0;
reg  [`PRIORITY_WIDTH-1:0] rsp_prio_reg = 0;
reg       [`PORTS_NUM-1:0] req_port_mask;
reg       [`PORTS_NUM-1:0] dst_port_mask;
reg                        s_axis_tvalid_q;
wire                       s_axis_tvalid;


t_forwarding_engine_request_decision_info s_axis_req_tdata_reg;
t_vtab_decision_info                      s_axis_vtab_tdata_reg;
t_mtab_decision_info                      s_axis_mtab_src_tdata_reg;
t_mtab_decision_info                      s_axis_mtab_dst_tdata_reg;
t_learning_fifo_entry                     s_axis_learning_fifo_tdata_reg;
t_tru_response                            s_axis_tru_tdata_reg;


// input data sets received (always in the same clock cycle)
assign s_axis_tvalid = S_axis_req_tvalid &&
                       S_axis_vtab_tvalid &&
                       S_axis_mtab_src_tvalid &&
                       S_axis_mtab_dst_tvalid &&
                       S_axis_learning_fifo_tvalid &&
                       (Tru_en ~^ S_axis_tru_tvalid);

                       // set one hot request port mask
assign req_port_mask = pid_to_port_mask(s_axis_req_tdata_reg.pid); 
                       // broadcast frame if HTAB entry for destination MAC address not found
assign dst_port_mask = (s_axis_mtab_dst_tdata_reg.found) ? s_axis_mtab_dst_tdata_reg.port_mask : '1; 
                   
always @ (posedge Clk)
    begin
        s_axis_tvalid_q <= s_axis_tvalid;
        M_axis_tvalid <= s_axis_tvalid_q;
        
        if (s_axis_tvalid)
            begin
                s_axis_req_tdata_reg           <= S_axis_req_tdata;       
                s_axis_vtab_tdata_reg          <= S_axis_vtab_tdata;       
                s_axis_mtab_src_tdata_reg      <= S_axis_mtab_src_tdata;   
                s_axis_mtab_dst_tdata_reg      <= S_axis_mtab_dst_tdata; 
                s_axis_learning_fifo_tdata_reg <= S_axis_learning_fifo_tdata;
                s_axis_tru_tdata_reg           <= S_axis_tru_tdata;      
            end
    end
// set response data set
always @ (posedge Clk)
    if (s_axis_tvalid_q)
        begin
            ////////////////////////////////////////////////
            // set request port identificator
            M_axis_tdest <= s_axis_req_tdata_reg.pid;
            
            ////////////////////////////////////////////////
            // for fututre use
            M_axis_tdata.rsvd <= '0;
            
            ////////////////////////////////////////////////
            // set response identificator
            M_axis_tdata.id <= s_axis_req_tdata_reg.id;
            
            ////////////////////////////////////////////////
            // set response high priority flag
            M_axis_tdata.hp <= 0; // for future use
            
            ////////////////////////////////////////////////
            // set response port mask
            rsp_port_mask_reg <= (Tru_en) ? s_axis_vtab_tdata_reg.port_mask & dst_port_mask & s_axis_tru_tdata_reg.port_mask :
                                            s_axis_vtab_tdata_reg.port_mask & dst_port_mask;
            
            ////////////////////////////////////////////////
            // set response priority
            if (s_axis_req_tdata_reg.has_prio)
                rsp_prio_reg <= s_axis_req_tdata_reg.prio;
            else if (s_axis_vtab_tdata_reg.has_prio && s_axis_vtab_tdata_reg.prio_override)
                rsp_prio_reg <= s_axis_vtab_tdata_reg.prio;
            else if (s_axis_mtab_src_tdata_reg.found && s_axis_mtab_src_tdata_reg.has_prio && s_axis_mtab_src_tdata_reg.prio_override)
                rsp_prio_reg <= s_axis_mtab_src_tdata_reg.prio;
            else if (s_axis_mtab_dst_tdata_reg.found && s_axis_mtab_dst_tdata_reg.has_prio && s_axis_mtab_dst_tdata_reg.prio_override)
                rsp_prio_reg <= s_axis_mtab_dst_tdata_reg.prio;
            else 
                rsp_prio_reg <= 0;
            
            ////////////////////////////////////////////////
            // set response drop flag
               
            // verify configuration settings
            if (~En)
                M_axis_tdata.drop <= 1;
            else if (~|(Pass_all & req_port_mask) && ~|(Pass_link_local & req_port_mask))
                M_axis_tdata.drop <= 1;
            // verify VTAB entry
            else if (s_axis_vtab_tdata_reg.drop)
                M_axis_tdata.drop <= 1;
            // verify source MAC entry
            else if (s_axis_mtab_src_tdata_reg.found && s_axis_mtab_src_tdata_reg.drop)
                M_axis_tdata.drop <= 1;
            else if (s_axis_mtab_src_tdata_reg.found && s_axis_mtab_src_tdata_reg.drop_unmatched_src && ~|(s_axis_mtab_src_tdata_reg.port_mask & req_port_mask))
                M_axis_tdata.drop <= 1;
            // verify destination MAC entry
            else if (~s_axis_mtab_dst_tdata_reg.found && ~|(B_unrec & req_port_mask))
                M_axis_tdata.drop <= 1;
            else if (s_axis_mtab_dst_tdata_reg.found && s_axis_mtab_dst_tdata_reg.drop)
                M_axis_tdata.drop <= 1;
            else if (s_axis_mtab_dst_tdata_reg.found && ~s_axis_mtab_dst_tdata_reg.is_link_local && ~|(Pass_all & req_port_mask))
                M_axis_tdata.drop <= 1;
            // verify TRU response
            else if (Tru_en && s_axis_tru_tdata_reg.drop)
                M_axis_tdata.drop <= 1;
            else
                M_axis_tdata.drop <= 0;

        end

// clear priority and port mask in response data set if drop flag set        
always @ *
    if (M_axis_tdata.drop)
        begin
            M_axis_tdata.prio      <= 0;
            M_axis_tdata.port_mask <= 0;
        end
    else
        begin
            M_axis_tdata.prio      <= rsp_prio_reg;
            M_axis_tdata.port_mask <= rsp_port_mask_reg;
        end
    
// put learning fifo entry into learning fifo if source MAC address not found and learning enable
// if there is no place in learning fifo (tready is not set) then the entry will not be put successfully
always @ (posedge Clk)
    if (s_axis_tvalid_q && ~s_axis_mtab_src_tdata_reg.found && |(Learn_en & req_port_mask))
        begin
            M_axis_learning_fifo_tvalid <= 1;
            M_axis_learning_fifo_tdata  <= s_axis_learning_fifo_tdata_reg;
        end
    else
        M_axis_learning_fifo_tvalid <= 0;

// set debug signal - FID & MAC not equal in HTAB and MTAB either for source MAC or for destination MAC
always @ (posedge Clk)
    dbg_mtab_unmatched <= s_axis_tvalid_q & (s_axis_mtab_src_tdata_reg.mtab_unmatched | s_axis_mtab_dst_tdata_reg.mtab_unmatched);
    
    
endmodule
