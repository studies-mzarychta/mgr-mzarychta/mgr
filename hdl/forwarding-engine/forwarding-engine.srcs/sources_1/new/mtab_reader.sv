//-----------------------------------------------------------------------------
// Title      : MAC table reader
// Project    : White Rabbit Switch
//-----------------------------------------------------------------------------
// File       : mtab_reader.sv
// Authors    : Magdalena Zarychta
// Company    : CERN BE-Co-HT
// Created    : 2021-09-15
// Last update: 2021-09-15
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//-----------------------------------------------------------------------------
// Description:
//
// The MTAB reader module reads a MAC table entry from the address contained 
// in a found HASH table entry. Its parameter is a MAC table read latency 
// given in clock cycles and a source/destination flag indicating which fields 
// from a read MAC table entry should be taken into account. The module 
// forwards a data set needed to make a final forwarding decision, including 
// a drop flag when a HASH table entry has not been found or when the FID and 
// the MAC address from a HASH table entry are non consistent with the FID
// and the MAC address from a MAC table entry. Additionally, the MTAB reader 
// instance dedicated for reading a source MAC entry updates the ageing bitmap 
// if a HASH table entry has been found.
// 
//-----------------------------------------------------------------------------
//
// Copyright (c) 2012 CERN / BE-CO-HT
//
// This source file is free software; you can redistribute it   
// and/or modify it under the terms of the GNU Lesser General   
// Public License as published by the Free Software Foundation; 
// either version 2.1 of the License, or (at your option) any   
// later version.                                               
//
// This source is distributed in the hope that it will be       
// useful, but WITHOUT ANY WARRANTY; without even the implied   
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
// PURPOSE.  See the GNU Lesser General Public License for more 
// details.                                                     
//
// You should have received a copy of the GNU Lesser General    
// Public License along with this source; if not, download it   
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author    Description
// 2021-09-15  1.0      mzarychta created
//-----------------------------------------------------------------------------

`timescale 1ns / 1ps

import forwarding_engine_pkg::*;

module mtab_reader #(
    parameter SOURCE = 1, // 1 - source, 0 - destination
    parameter [1:0] MTAB_READ_LATENCY = 1 // available values: 1/2
) (

    input Clk,
    input Rst,
    
    input                              S_axis_tvalid,
    input                 t_htab_entry S_axis_tdata,
    input                              S_axis_tuser, // 1 - found, 0 - not found
    
    output                             M_axis_tvalid,
    output        t_mtab_decision_info M_axis_tdata,
    
    output      [`MTAB_ADDR_WIDTH-1:0] Mtab_addr,
    input                 t_mtab_entry Mtab_dout,
    
    output                             Ageing_bitmap_web,
    output      [`MTAB_ADDR_WIDTH-1:0] Ageing_bitmap_addrb,
    output                             Ageing_bitmap_dinb

);

t_htab_entry axis_tdata_reg = 0;
t_htab_entry axis_tdata_reg_q = 0;
reg          axis_tuser_reg = 0;
reg          axis_tuser_reg_q = 0;
reg          axis_tvalid_reg = 0;
reg          axis_tvalid_reg_q = 0;

assign Mtab_addr = S_axis_tdata.ptr;  

generate
if (MTAB_READ_LATENCY == 1) begin
    always @ (posedge Clk)
        if (Rst)
            axis_tvalid_reg <= 0;
        else
            begin
                axis_tvalid_reg <= S_axis_tvalid;
                axis_tdata_reg <= S_axis_tdata;
                axis_tuser_reg <= S_axis_tuser;
            end
end
else if (MTAB_READ_LATENCY == 2) begin
    always @ (posedge Clk)
        if (Rst)
            axis_tvalid_reg <= 0;
        else
            begin
                axis_tvalid_reg_q <= S_axis_tvalid;
                axis_tdata_reg_q <= S_axis_tdata;
                axis_tuser_reg_q <= S_axis_tuser;
                
                axis_tvalid_reg <= axis_tvalid_reg_q;
                axis_tdata_reg <= axis_tdata_reg_q;
                axis_tuser_reg <= axis_tuser_reg_q;
            end 
end
endgenerate
        
assign M_axis_tvalid = axis_tvalid_reg; 
    
assign M_axis_tdata.found              = axis_tuser_reg && ~M_axis_tdata.mtab_unmatched;             
assign M_axis_tdata.mtab_unmatched     = (axis_tdata_reg.fid != Mtab_dout.fid) || (axis_tdata_reg.mac != Mtab_dout.mac);         
assign M_axis_tdata.port_mask          = (SOURCE) ? Mtab_dout.port_mask_src[`PORTS_NUM-1:0] : Mtab_dout.port_mask_dst[`PORTS_NUM-1:0];
assign M_axis_tdata.drop               = (SOURCE) ? Mtab_dout.drop_when_src : Mtab_dout.drop_when_dst;             
assign M_axis_tdata.drop_unmatched_src = Mtab_dout.drop_unmatched_src;
assign M_axis_tdata.has_prio           = (SOURCE) ? Mtab_dout.has_prio_src : Mtab_dout.has_prio_dst;          
assign M_axis_tdata.prio_override      = (SOURCE) ? Mtab_dout.prio_override_src : Mtab_dout.prio_override_dst;    
assign M_axis_tdata.prio               = (SOURCE) ? Mtab_dout.prio_src : Mtab_dout.prio_dst;            
assign M_axis_tdata.is_link_local      = Mtab_dout.is_link_local; 
assign M_axis_tdata.rsvd               = '0; // for future use 

assign Ageing_bitmap_web   = axis_tvalid_reg && M_axis_tdata.found;
assign Ageing_bitmap_addrb = axis_tdata_reg.ptr;
assign Ageing_bitmap_dinb  = 'b1;  


endmodule