interface axi4stream_if #(int TDATA_WIDTH = 0) 
(input bit clk, rst);
    
logic                   tvalid;
logic                   tready;
logic [TDATA_WIDTH-1:0] tdata;

modport master (input tready, output tvalid, tdata);
modport slave (input tvalid, tdata, output tready);
modport monitor (input tvalid, tready, tdata);

`ifdef XILINX_SIMULATOR
clocking cb @(posedge clk);
endclocking
`endif

endinterface