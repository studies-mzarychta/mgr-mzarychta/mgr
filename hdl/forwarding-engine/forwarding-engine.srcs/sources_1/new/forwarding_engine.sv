//-----------------------------------------------------------------------------
// Title      : Forwarding Engine
// Project    : White Rabbit Switch
//-----------------------------------------------------------------------------
// File       : forwarding_engine.sv
// Authors    : Magdalena Zarychta
// Company    : CERN BE-Co-HT
// Created    : 2021-09-15
// Last update: 2021-09-15
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//-----------------------------------------------------------------------------
// Description:
//
// Forwarding engine decides to which ports a frame should be sent. 
// In general the decision is based on the VLAN table (VTAB) record that matches
// VLAN ID and on the HASH table (HTAB, MTAB) record that matches MAC addresses 
// and the Filtering ID read previously from the VLAN table.
//
// More details are included in submodules' descriptions.
// 
//-----------------------------------------------------------------------------
//
// Copyright (c) 2012 CERN / BE-CO-HT
//
// This source file is free software; you can redistribute it   
// and/or modify it under the terms of the GNU Lesser General   
// Public License as published by the Free Software Foundation; 
// either version 2.1 of the License, or (at your option) any   
// later version.                                               
//
// This source is distributed in the hope that it will be       
// useful, but WITHOUT ANY WARRANTY; without even the implied   
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
// PURPOSE.  See the GNU Lesser General Public License for more 
// details.                                                     
//
// You should have received a copy of the GNU Lesser General    
// Public License along with this source; if not, download it   
// from http://www.gnu.org/licenses/lgpl-2.1.html
//
//-----------------------------------------------------------------------------
// Revisions  :
// Date        Version  Author    Description
// 2021-09-15  1.0      mzarychta created
//-----------------------------------------------------------------------------

`timescale 1ns / 1ps

import forwarding_engine_pkg::*;

module forwarding_engine #(
    parameter [`PORT_ID_WIDTH-1:0] PORT_ID [`PORTS_NUM_PER_ENGINE-1:0] = '{23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0}) (

    input Clk,
    input Rst,
    
    // global configuration
    input En,                         // forwarding processing enabled
    input Tru_en,                     // TRU processing enabled
    input Ageing_bitmap_ptr,          // pointer to currently active ageing bitmap

    // per port configuration
    input [`PORTS_NUM-1:0] Pass_all ,         // pass frames received on given port    
    input [`PORTS_NUM-1:0] Pass_link_local,   // pass frames with link-local address received on a given port
    input [`PORTS_NUM-1:0] B_unrec,           // if 1, broadcast unrecognized frames received on a given port; 
                                              // if 0, drop unrecognized frames received on a given port
    input [`PORTS_NUM-1:0] Learn_en,          // if 1, learning enable on a given port
    
    axi4stream_if.slave                 S_axis[`PORTS_NUM_PER_ENGINE-1:0],
    axi4stream_if.master                M_axis[`PORTS_NUM_PER_ENGINE-1:0],
    axi4stream_if.slave                 S_axis_tru,
    axi4stream_if.master                M_axis_tru,
    axi4stream_if.master                M_axis_learning_fifo,
    
    input                               Vtab_wea,
    input        [`VTAB_ADDR_WIDTH-1:0] Vtab_addra,
    input        [`VTAB_DATA_WIDTH-1:0] Vtab_dina,
    
    input                               Htab_src_wea [`HTAB_SUBTABLES_NUM-1:0],
    input        [`HTAB_ADDR_WIDTH-1:0] Htab_src_addra [`HTAB_SUBTABLES_NUM-1:0],
    input        [`HTAB_DATA_WIDTH-1:0] Htab_src_dina [`HTAB_SUBTABLES_NUM-1:0],
    
    input                               Htab_dst_wea [`HTAB_SUBTABLES_NUM-1:0],
    input        [`HTAB_ADDR_WIDTH-1:0] Htab_dst_addra [`HTAB_SUBTABLES_NUM-1:0],
    input        [`HTAB_DATA_WIDTH-1:0] Htab_dst_dina [`HTAB_SUBTABLES_NUM-1:0],
    
    input                               Mtab_src_wea,
    input        [`MTAB_ADDR_WIDTH-1:0] Mtab_src_addra,
    input        [`MTAB_DATA_WIDTH-1:0] Mtab_src_dina,
    
    input                               Mtab_dst_wea,
    input        [`MTAB_ADDR_WIDTH-1:0] Mtab_dst_addra,
    input        [`MTAB_DATA_WIDTH-1:0] Mtab_dst_dina,
    
    input                               Ageing_bitmap_wea,
    input        [`MTAB_ADDR_WIDTH-1:0] Ageing_bitmap_addra,
    input                               Ageing_bitmap_dina,
    output                              Ageing_bitmap_douta,
    
    output                              dbg_mtab_unmatched,            // if 1, FID & MAC not equal in HTAB and MTAB
    output                              dbg_learning_fifo_almost_full, // if 1, learning fifo is almost full
    output reg                          dbg_axis_learning_fifo_tready, // if 1, learning fifo entry is full and entries may be lost
    output reg                          dbg_axis_decision_maker_tready // if 1, no tready on forwarding engines output and responses may be lost

);
    
wire                                           axis_input_reg_slice_tready [`PORTS_NUM_PER_ENGINE-1:0];
wire                                           axis_input_reg_slice_tvalid [`PORTS_NUM_PER_ENGINE-1:0];
wire               t_forwarding_engine_request axis_input_reg_slice_tdata [`PORTS_NUM_PER_ENGINE-1:0];
wire                      [`PORT_ID_WIDTH-1:0] axis_input_reg_slice_tdest [`PORTS_NUM_PER_ENGINE-1:0];

wire                                           axis_input_switch_tready [3];
wire                                           axis_input_switch_tvalid [3];
wire               t_forwarding_engine_request axis_input_switch_tdata [3];
wire                      [`PORT_ID_WIDTH-1:0] axis_input_switch_tdest [3];

wire                                           axis_vtab_reader_htab_src_info_tvalid;
wire                               t_htab_info axis_vtab_reader_htab_src_info_tdata;

wire                                           axis_vtab_reader_htab_dst_info_tvalid;
wire                               t_htab_info axis_vtab_reader_htab_dst_info_tdata;

wire                                           axis_reg_slice_req_info_tvalid [`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1:0];
wire t_forwarding_engine_request_decision_info axis_reg_slice_req_info_tdata [`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1:0];

wire                                           axis_reg_slice_vtab_info_tvalid [`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1:0];
wire                      t_vtab_decision_info axis_reg_slice_vtab_info_tdata [`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1:0];

wire                                           axis_reg_slice_learning_fifo_tvalid [`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1:0];
wire                     t_learning_fifo_entry axis_reg_slice_learning_fifo_tdata [`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1:0];

wire                                           axis_htab_lookup_src_tvalid;
wire                              t_htab_entry axis_htab_lookup_src_tdata;
wire                                           axis_htab_lookup_src_tuser;

wire                                           axis_htab_lookup_dst_tvalid;
wire                              t_htab_entry axis_htab_lookup_dst_tdata;  
wire                                           axis_htab_lookup_dst_tuser;

wire                                           axis_mtab_lookup_src_tvalid;
wire                              t_htab_entry axis_mtab_lookup_src_tdata;
wire                                           axis_mtab_lookup_src_tuser;

wire                                           axis_mtab_lookup_dst_tvalid;
wire                              t_htab_entry axis_mtab_lookup_dst_tdata;  
wire                                           axis_mtab_lookup_dst_tuser;

wire                                           axis_mtab_reader_src_tvalid;
wire                      t_mtab_decision_info axis_mtab_reader_src_tdata;

wire                                           axis_mtab_reader_dst_tvalid;
wire                      t_mtab_decision_info axis_mtab_reader_dst_tdata;

wire                                           axis_decision_maker_tready;
wire                                           axis_decision_maker_tvalid;
wire              t_forwarding_engine_response axis_decision_maker_tdata;
wire                      [`PORT_ID_WIDTH-1:0] axis_decision_maker_tdest;

wire                                           axis_output_switch_tready [2];
wire                                           axis_output_switch_tvalid [2];
wire              t_forwarding_engine_response axis_output_switch_tdata [2];
wire                      [`PORT_ID_WIDTH-1:0] axis_output_switch_tdest [2];

wire                                           axis_output_reg_slice_tready [`PORTS_NUM_PER_ENGINE-1:0];
wire                                           axis_output_reg_slice_tvalid [`PORTS_NUM_PER_ENGINE-1:0];
wire              t_forwarding_engine_response axis_output_reg_slice_tdata [`PORTS_NUM_PER_ENGINE-1:0];

wire                                           axis_learning_fifo_tready;
wire                                           axis_learning_fifo_tvalid;
wire                     t_learning_fifo_entry axis_learning_fifo_tdata;
wire           [`LEARNING_FIFO_DATA_WIDTH-1:0] m_axis_learning_fifo_tdata;
    
    
wire        [`VTAB_ADDR_WIDTH-1:0] vtab_addrb;
wire        [`VTAB_DATA_WIDTH-1:0] vtab_doutb;

wire        [`HTAB_ADDR_WIDTH-1:0] htab_src_addrb [`HTAB_SUBTABLES_NUM-1:0];
wire        [`HTAB_DATA_WIDTH-1:0] htab_src_doutb [`HTAB_SUBTABLES_NUM-1:0];
wire                  t_htab_entry htab_src_doutb_entry [`HTAB_SUBTABLES_NUM-1:0];
wire        [`HTAB_ADDR_WIDTH-1:0] htab_dst_addrb [`HTAB_SUBTABLES_NUM-1:0];
wire        [`HTAB_DATA_WIDTH-1:0] htab_dst_doutb [`HTAB_SUBTABLES_NUM-1:0];
wire                  t_htab_entry htab_dst_doutb_entry [`HTAB_SUBTABLES_NUM-1:0];

wire        [`MTAB_ADDR_WIDTH-1:0] mtab_src_addrb;
wire        [`MTAB_DATA_WIDTH-1:0] mtab_src_doutb;
wire        [`MTAB_ADDR_WIDTH-1:0] mtab_dst_addrb;
wire        [`MTAB_DATA_WIDTH-1:0] mtab_dst_doutb;

wire                               ageing_bitmap_web;
wire        [`MTAB_ADDR_WIDTH-1:0] ageing_bitmap_addrb;
wire                               ageing_bitmap_dinb;
wire                               ageing_bitmap_douta0;
wire                               ageing_bitmap_douta1;

//////////////////////////////////////////////////////////////////////////////////
// debug signals
always @ (posedge Clk)
    begin
        dbg_axis_learning_fifo_tready <= ~axis_learning_fifo_tready;
        dbg_axis_decision_maker_tready <= ~axis_decision_maker_tready;
    end

//////////////////////////////////////////////////////////////////////////////////
// forwarding engine input reg slices

generate
begin : input_reg_slices
for (genvar i = 0; i < `PORTS_NUM_PER_ENGINE; i++) begin : input_reg_slice
    axis_register_slice_128b input_reg_slice_inst (
      .aclk(Clk),
      .aresetn(~Rst),
      .s_axis_tvalid(S_axis[i].tvalid),
      .s_axis_tready(S_axis[i].tready),
      .s_axis_tdata(S_axis[i].tdata),
      .m_axis_tvalid(axis_input_reg_slice_tvalid[i]),
      .m_axis_tready(axis_input_reg_slice_tready[i]), 
      .m_axis_tdata(axis_input_reg_slice_tdata[i])
    );
    
    assign axis_input_reg_slice_tdest[i] = PORT_ID[i];
end
end
endgenerate

//////////////////////////////////////////////////////////////////////////////////
// forwarding engine input switches

axis_switch_12x1_128b_tdest5b input_switch_12x1_inst0 (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid({<<{axis_input_reg_slice_tvalid[11:0]}}),
  .s_axis_tready({<<{axis_input_reg_slice_tready[11:0]}}),
  .s_axis_tdata({<<{axis_input_reg_slice_tdata[11:0]}}),
  .s_axis_tdest({<<{axis_input_reg_slice_tdest[11:0]}}),
  .m_axis_tvalid(axis_input_switch_tvalid[0]),
  .m_axis_tready(axis_input_switch_tready[0]),
  .m_axis_tdata(axis_input_switch_tdata[0]),
  .m_axis_tdest(axis_input_switch_tdest[0]),
  .s_req_suppress(0),
  .s_decode_err()
);

axis_switch_12x1_128b_tdest5b input_switch_12x1_inst1 (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid({<<{axis_input_reg_slice_tvalid[23:12]}}),
  .s_axis_tready({<<{axis_input_reg_slice_tready[23:12]}}),
  .s_axis_tdata({<<{axis_input_reg_slice_tdata[23:12]}}),
  .s_axis_tdest({<<{axis_input_reg_slice_tdest[23:12]}}),
  .m_axis_tvalid(axis_input_switch_tvalid[1]),
  .m_axis_tready(axis_input_switch_tready[1]),
  .m_axis_tdata(axis_input_switch_tdata[1]),
  .m_axis_tdest(axis_input_switch_tdest[1]),
  .s_req_suppress(0),
  .s_decode_err()
);

axis_switch_2x1_128b_tdest5b input_switch_2x1_inst (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid({axis_input_switch_tvalid[1], axis_input_switch_tvalid[0]}),
  .s_axis_tready({axis_input_switch_tready[1], axis_input_switch_tready[0]}),
  .s_axis_tdata({axis_input_switch_tdata[1], axis_input_switch_tdata[0]}),
  .s_axis_tdest({axis_input_switch_tdest[1], axis_input_switch_tdest[0]}),
  .m_axis_tvalid(axis_input_switch_tvalid[2]),
  .m_axis_tready(axis_input_switch_tready[2]),
  .m_axis_tdata(axis_input_switch_tdata[2]),
  .m_axis_tdest(axis_input_switch_tdest[2]),
  .s_req_suppress(0),
  .s_decode_err()
);

//////////////////////////////////////////////////////////////////////////////////
// vtab memory

xpm_memory_sdpram #(
    .ADDR_WIDTH_A(`VTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`VTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(`VTAB_DATA_WIDTH), // word-wide writes
    .CLOCKING_MODE("common_clock"), // common_clock / independent_clock
    `ifdef XILINX_SIMULATOR
    .MEMORY_INIT_FILE("vtab.mem"),
    `endif
    .MEMORY_INIT_PARAM(""),
    .MEMORY_PRIMITIVE("auto"), // auto / block / distributed / ultra
    .MEMORY_SIZE(`VTAB_DEPTH*`VTAB_DATA_WIDTH),
    .READ_DATA_WIDTH_B(`VTAB_DATA_WIDTH), // word-wide reads
    .READ_LATENCY_B(`VTAB_READ_LATENCY),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0),
    .USE_EMBEDDED_CONSTRAINT(0),
    .WRITE_DATA_WIDTH_A(`VTAB_DATA_WIDTH))
vtab_inst (
    .dbiterrb(),
    .doutb(vtab_doutb),
    .sbiterrb(),
    .addra(Vtab_addra),
    .addrb(vtab_addrb),
    .clka(Clk),
    .clkb(Clk),
    .dina(Vtab_dina),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectsbiterra('0),
    .regceb('1),
    .rstb(Rst),
    .sleep('0),
    .wea(Vtab_wea)
);

//////////////////////////////////////////////////////////////////////////////////
// vtab reader  
    
assign axis_input_switch_tready[2] = 1;
    
vtab_reader #(.VTAB_READ_LATENCY(`VTAB_READ_LATENCY))
vtab_reader_inst (
    .Clk(Clk),
    .Rst(Rst),
    .S_axis_tvalid(axis_input_switch_tvalid[2]),
    .S_axis_tdata(axis_input_switch_tdata[2]),
    .S_axis_tdest(axis_input_switch_tdest[2]), 
    .M_axis_req_tvalid(axis_reg_slice_req_info_tvalid[0]),
    .M_axis_req_tdata(axis_reg_slice_req_info_tdata[0]), 
    .M_axis_vtab_tvalid(axis_reg_slice_vtab_info_tvalid[0]),
    .M_axis_vtab_tdata(axis_reg_slice_vtab_info_tdata[0]),
    .M_axis_htab_src_tvalid(axis_vtab_reader_htab_src_info_tvalid),
    .M_axis_htab_src_tdata(axis_vtab_reader_htab_src_info_tdata),
    .M_axis_htab_dst_tvalid(axis_vtab_reader_htab_dst_info_tvalid),
    .M_axis_htab_dst_tdata(axis_vtab_reader_htab_dst_info_tdata),
    .M_axis_learning_fifo_tvalid(axis_reg_slice_learning_fifo_tvalid[0]),
    .M_axis_learning_fifo_tdata(axis_reg_slice_learning_fifo_tdata[0]),
    .M_axis_tru_tvalid(M_axis_tru.tvalid),
    .M_axis_tru_tdata(M_axis_tru.tdata),
    .Vtab_addr(vtab_addrb),
    .Vtab_dout(vtab_doutb[$size(t_vtab_entry)-1:0])
);

//////////////////////////////////////////////////////////////////////////////////
// vtab reader - forwarding engine decision maker reg slices

generate
begin : decision_maker_reg_slices
for (genvar i = 0; i < `HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1; i++) begin : decision_maker_reg_slice

    axis_register_slice_24b vtab_reader_req_info_reg_slice (
      .aclk(Clk),
      .aresetn(~Rst),
      .s_axis_tvalid(axis_reg_slice_req_info_tvalid[i]),
      .s_axis_tdata(axis_reg_slice_req_info_tdata[i]),
      .m_axis_tvalid(axis_reg_slice_req_info_tvalid[i+1]),
      .m_axis_tdata(axis_reg_slice_req_info_tdata[i+1])
    );
    
    axis_register_slice_32b vtab_reader_vtab_info_reg_slice (
      .aclk(Clk),
      .aresetn(~Rst),
      .s_axis_tvalid(axis_reg_slice_vtab_info_tvalid[i]),
      .s_axis_tdata(axis_reg_slice_vtab_info_tdata[i]),
      .m_axis_tvalid(axis_reg_slice_vtab_info_tvalid[i+1]),
      .m_axis_tdata(axis_reg_slice_vtab_info_tdata[i+1])
    );
    
    axis_register_slice_72b vtab_reader_learning_fifo_reg_slice (
      .aclk(Clk),
      .aresetn(~Rst),
      .s_axis_tvalid(axis_reg_slice_learning_fifo_tvalid[i]),
      .s_axis_tdata(axis_reg_slice_learning_fifo_tdata[i]),
      .m_axis_tvalid(axis_reg_slice_learning_fifo_tvalid[i+1]),
      .m_axis_tdata(axis_reg_slice_learning_fifo_tdata[i+1])
    );
end
end
endgenerate

//////////////////////////////////////////////////////////////////////////////////
// htab memory
    
generate
begin
for (genvar i = 0; i < `HTAB_SUBTABLES_NUM; i++) begin : htab_subtable
xpm_memory_sdpram #(
    .ADDR_WIDTH_A(`HTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`HTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(`HTAB_DATA_WIDTH), // word-wide writes
    .CLOCKING_MODE("common_clock"), // common_clock / independent_clock
    `ifdef XILINX_SIMULATOR
    .MEMORY_INIT_FILE($sformatf("htab_%0d.mem",i)),
    `endif
    .MEMORY_PRIMITIVE("ultra"), // auto / block / distributed / ultra
    .MEMORY_SIZE(`HTAB_DEPTH*`HTAB_DATA_WIDTH),
    .READ_DATA_WIDTH_B(`HTAB_DATA_WIDTH), // word-wide reads
    .READ_LATENCY_B(`HTAB_READ_LATENCY),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0),
    .USE_EMBEDDED_CONSTRAINT(0),
    .WRITE_DATA_WIDTH_A(`HTAB_DATA_WIDTH),
    .WRITE_MODE_B("read_first"))
htab_subtable_src_inst (
    .dbiterrb(),
    .doutb(htab_src_doutb[i]),
    .sbiterrb(),
    .addra(Htab_src_addra[i]),
    .addrb(htab_src_addrb[i]),
    .clka(Clk),
    .clkb(Clk),
    .dina(Htab_src_dina[i]),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectsbiterra('0),
    .regceb('1),
    .rstb(Rst),
    .sleep('0),
    .wea(Htab_src_wea[i])
);

xpm_memory_sdpram #(
    .ADDR_WIDTH_A(`HTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`HTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(`HTAB_DATA_WIDTH), // word-wide writes
    .CLOCKING_MODE("common_clock"), // common_clock / independent_clock
    `ifdef XILINX_SIMULATOR
    .MEMORY_INIT_FILE($sformatf("htab_%0d.mem",i)),
    `endif
    .MEMORY_PRIMITIVE("ultra"), // auto / block / distributed / ultra
    .MEMORY_SIZE(`HTAB_DEPTH*`HTAB_DATA_WIDTH),
    .READ_DATA_WIDTH_B(`HTAB_DATA_WIDTH), // word-wide reads
    .READ_LATENCY_B(`HTAB_READ_LATENCY),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0),
    .USE_EMBEDDED_CONSTRAINT(0),
    .WRITE_DATA_WIDTH_A(`HTAB_DATA_WIDTH),
    .WRITE_MODE_B("read_first"))
htab_subtable_dst_inst (
    .dbiterrb(),
    .doutb(htab_dst_doutb[i]),
    .sbiterrb(),
    .addra(Htab_dst_addra[i]),
    .addrb(htab_dst_addrb[i]),
    .clka(Clk),
    .clkb(Clk),
    .dina(Htab_dst_dina[i]),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectsbiterra('0),
    .regceb('1),
    .rstb(Rst),
    .sleep('0),
    .wea(Htab_dst_wea[i])
);

assign htab_src_doutb_entry[i] = htab_src_doutb[i][$size(t_htab_entry)-1:0];
assign htab_dst_doutb_entry[i] = htab_dst_doutb[i][$size(t_htab_entry)-1:0];
end
end
endgenerate

//////////////////////////////////////////////////////////////////////////////////
// htab source mac lookup

htab_lookup #(.HTAB_READ_LATENCY(`HTAB_READ_LATENCY))
htab_lookup_src_inst (
    .Clk(Clk),
    .Rst(Rst),
    .S_axis_tvalid(axis_vtab_reader_htab_src_info_tvalid),
    .S_axis_tdata(axis_vtab_reader_htab_src_info_tdata),
    .M_axis_tvalid(axis_htab_lookup_src_tvalid),
    .M_axis_tdata(axis_htab_lookup_src_tdata),
    .M_axis_tuser(axis_htab_lookup_src_tuser),
    .Htab_addr(htab_src_addrb),
    .Htab_dout(htab_src_doutb_entry)
);

//////////////////////////////////////////////////////////////////////////////////
// htab dst mac lookup

htab_lookup #(.HTAB_READ_LATENCY(`HTAB_READ_LATENCY))
htab_lookup_dst_inst (
    .Clk(Clk),
    .Rst(Rst),
    .S_axis_tvalid(axis_vtab_reader_htab_dst_info_tvalid),
    .S_axis_tdata(axis_vtab_reader_htab_dst_info_tdata),
    .M_axis_tvalid(axis_htab_lookup_dst_tvalid),
    .M_axis_tdata(axis_htab_lookup_dst_tdata),
    .M_axis_tuser(axis_htab_lookup_dst_tuser),
    .Htab_addr(htab_dst_addrb),
    .Htab_dout(htab_dst_doutb_entry)
);

//////////////////////////////////////////////////////////////////////////////////
// mtab memory
    
xpm_memory_sdpram #(
    .ADDR_WIDTH_A(`MTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`MTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(`MTAB_DATA_WIDTH), // word-wide writes
    .CLOCKING_MODE("common_clock"), // common_clock / independent_clock
    `ifdef XILINX_SIMULATOR
    .MEMORY_INIT_FILE("mtab.mem"),
    `endif
    .MEMORY_PRIMITIVE("auto"), // auto / block / distributed / ultra
    .MEMORY_SIZE(`MTAB_DEPTH*`MTAB_DATA_WIDTH),
    .READ_DATA_WIDTH_B(`MTAB_DATA_WIDTH), // word-wide reads
    .READ_LATENCY_B(`MTAB_READ_LATENCY),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0),
    .USE_EMBEDDED_CONSTRAINT(0),
    .WRITE_DATA_WIDTH_A(`MTAB_DATA_WIDTH))
mtab_src_inst (
    .dbiterrb(),
    .doutb(mtab_src_doutb),
    .sbiterrb(),
    .addra(Mtab_src_addra),
    .addrb(mtab_src_addrb),
    .clka(Clk),
    .clkb(Clk),
    .dina(Mtab_src_dina),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectsbiterra('0),
    .regceb('1),
    .rstb(Rst),
    .sleep('0),
    .wea(Mtab_src_wea)
);

xpm_memory_sdpram #(
    .ADDR_WIDTH_A(`MTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`MTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(`MTAB_DATA_WIDTH), // word-wide writes
    .CLOCKING_MODE("common_clock"), // common_clock / independent_clock
    `ifdef XILINX_SIMULATOR
    .MEMORY_INIT_FILE("mtab.mem"),
    `endif
    .MEMORY_PRIMITIVE("auto"), // auto / block / distributed / ultra
    .MEMORY_SIZE(`MTAB_DEPTH*`MTAB_DATA_WIDTH),
    .READ_DATA_WIDTH_B(`MTAB_DATA_WIDTH), // word-wide reads
    .READ_LATENCY_B(`MTAB_READ_LATENCY),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0),
    .USE_EMBEDDED_CONSTRAINT(0),
    .WRITE_DATA_WIDTH_A(`MTAB_DATA_WIDTH))
mtab_dst_inst (
    .dbiterrb(),
    .doutb(mtab_dst_doutb),
    .sbiterrb(),
    .addra(Mtab_dst_addra),
    .addrb(mtab_dst_addrb),
    .clka(Clk),
    .clkb(Clk),
    .dina(Mtab_dst_dina),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectsbiterra('0),
    .regceb('1),
    .rstb(Rst),
    .sleep('0),
    .wea(Mtab_dst_wea)
);

//////////////////////////////////////////////////////////////////////////////////
// mtab source mac reader

axis_register_slice_72b_tuser1b mtab_reader_src_reg_slice_inst (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid(axis_htab_lookup_src_tvalid),
  .s_axis_tready(),
  .s_axis_tdata(axis_htab_lookup_src_tdata),
  .s_axis_tuser(axis_htab_lookup_src_tuser),
  .m_axis_tvalid(axis_mtab_lookup_src_tvalid),
  .m_axis_tready('1),
  .m_axis_tdata(axis_mtab_lookup_src_tdata),
  .m_axis_tuser(axis_mtab_lookup_src_tuser)
);

mtab_reader #(.SOURCE(1), .MTAB_READ_LATENCY(`MTAB_READ_LATENCY))
mtab_reader_src_inst (
    .Clk(Clk),
    .Rst(Rst),
    .S_axis_tvalid(axis_mtab_lookup_src_tvalid),
    .S_axis_tdata(axis_mtab_lookup_src_tdata),
    .S_axis_tuser(axis_mtab_lookup_src_tuser),
    .M_axis_tvalid(axis_mtab_reader_src_tvalid),
    .M_axis_tdata(axis_mtab_reader_src_tdata),
    .Mtab_addr(mtab_src_addrb),
    .Mtab_dout(mtab_src_doutb[$size(t_mtab_entry)-1:0]),
    .Ageing_bitmap_web(ageing_bitmap_web),
    .Ageing_bitmap_addrb(ageing_bitmap_addrb),
    .Ageing_bitmap_dinb(ageing_bitmap_dinb)
);

//////////////////////////////////////////////////////////////////////////////////
// mtab dst mac reader

axis_register_slice_72b_tuser1b mtab_reader_dst_reg_slice_inst (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid(axis_htab_lookup_dst_tvalid),
  .s_axis_tready(),
  .s_axis_tdata(axis_htab_lookup_dst_tdata),
  .s_axis_tuser(axis_htab_lookup_dst_tuser),
  .m_axis_tvalid(axis_mtab_lookup_dst_tvalid),
  .m_axis_tready('1),
  .m_axis_tdata(axis_mtab_lookup_dst_tdata),
  .m_axis_tuser(axis_mtab_lookup_dst_tuser)
);

mtab_reader #(.SOURCE(0), .MTAB_READ_LATENCY(`MTAB_READ_LATENCY))
mtab_reader_dst_inst (
    .Clk(Clk),
    .Rst(Rst),
    .S_axis_tvalid(axis_mtab_lookup_dst_tvalid),
    .S_axis_tdata(axis_mtab_lookup_dst_tdata),
    .S_axis_tuser(axis_mtab_lookup_dst_tuser),
    .M_axis_tvalid(axis_mtab_reader_dst_tvalid),
    .M_axis_tdata(axis_mtab_reader_dst_tdata),
    .Mtab_addr(mtab_dst_addrb),
    .Mtab_dout(mtab_dst_doutb[$size(t_mtab_entry)-1:0]),
    .Ageing_bitmap_web(),
    .Ageing_bitmap_addrb(),
    .Ageing_bitmap_dinb()
);

//////////////////////////////////////////////////////////////////////////////////
// ageing bitmaps

xpm_memory_tdpram #(
    .ADDR_WIDTH_A(`MTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`MTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(1),
    .BYTE_WRITE_WIDTH_B(1),
    .MEMORY_SIZE(`MTAB_DEPTH),
    .READ_DATA_WIDTH_A(1),
    .READ_DATA_WIDTH_B(1),
    .WRITE_DATA_WIDTH_A(1),
    .WRITE_DATA_WIDTH_B(1),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0)
)
ageing_bitmap_inst0 (
    .dbiterra(),
    .dbiterrb(),
    .douta(ageing_bitmap_douta0),
    .doutb(),
    .sbiterra(),
    .sbiterrb(),
    .addra(Ageing_bitmap_addra),
    .addrb(ageing_bitmap_addrb),
    .clka(Clk),
    .clkb(Clk),
    .dina(Ageing_bitmap_dina),
    .dinb(ageing_bitmap_dinb),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectdbiterrb('0),
    .injectsbiterra('0),
    .injectsbiterrb('0),
    .regcea('1),
    .regceb('1),
    .rsta(Rst),
    .rstb(Rst),
    .sleep('0),
    .wea(Ageing_bitmap_wea && Ageing_bitmap_ptr),
    .web(ageing_bitmap_web && ~Ageing_bitmap_ptr)
);

xpm_memory_tdpram #(
    .ADDR_WIDTH_A(`MTAB_ADDR_WIDTH),
    .ADDR_WIDTH_B(`MTAB_ADDR_WIDTH),
    .BYTE_WRITE_WIDTH_A(1),
    .BYTE_WRITE_WIDTH_B(1),
    .MEMORY_SIZE(`MTAB_DEPTH),
    .READ_DATA_WIDTH_A(1),
    .READ_DATA_WIDTH_B(1),
    .WRITE_DATA_WIDTH_A(1),
    .WRITE_DATA_WIDTH_B(1),
    .SIM_ASSERT_CHK(0),
    .USE_MEM_INIT(0)
)
ageing_bitmap_inst1 (
    .dbiterra(),
    .dbiterrb(),
    .douta(ageing_bitmap_douta1),
    .doutb(),
    .sbiterra(),
    .sbiterrb(),
    .addra(Ageing_bitmap_addra),
    .addrb(ageing_bitmap_addrb),
    .clka(Clk),
    .clkb(Clk),
    .dina(Ageing_bitmap_dina),
    .dinb(ageing_bitmap_dinb),
    .ena('1),
    .enb('1),
    .injectdbiterra('0),
    .injectdbiterrb('0),
    .injectsbiterra('0),
    .injectsbiterrb('0),
    .regcea('1),
    .regceb('1),
    .rsta(Rst),
    .rstb(Rst),
    .sleep('0),
    .wea(Ageing_bitmap_wea && ~Ageing_bitmap_ptr),
    .web(ageing_bitmap_web && Ageing_bitmap_ptr)
);

assign Ageing_bitmap_douta = (Ageing_bitmap_ptr) ? ageing_bitmap_douta0 : ageing_bitmap_douta1;

//////////////////////////////////////////////////////////////////////////////////
// forwarding decision maker

forwarding_engine_decision_maker forwarding_engine_decision_maker_inst (
    .Clk(Clk),
    .Rst(Rst),
    .En(En),
    .Tru_en(Tru_en),
    .Pass_all(Pass_all), 
    .Pass_link_local(Pass_link_local),
    .B_unrec(B_unrec),
    .Learn_en(Learn_en),
    .S_axis_req_tvalid(axis_reg_slice_req_info_tvalid[`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1]),
    .S_axis_req_tdata(axis_reg_slice_req_info_tdata[`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1]),
    .S_axis_vtab_tvalid(axis_reg_slice_vtab_info_tvalid[`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1]),
    .S_axis_vtab_tdata(axis_reg_slice_vtab_info_tdata[`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1]),
    .S_axis_mtab_src_tvalid(axis_mtab_reader_src_tvalid),
    .S_axis_mtab_src_tdata(axis_mtab_reader_src_tdata),
    .S_axis_mtab_dst_tvalid(axis_mtab_reader_dst_tvalid),
    .S_axis_mtab_dst_tdata(axis_mtab_reader_dst_tdata),
    .S_axis_learning_fifo_tvalid(axis_reg_slice_learning_fifo_tvalid[`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1]),
    .S_axis_learning_fifo_tdata(axis_reg_slice_learning_fifo_tdata[`HTAB_READ_LATENCY+`MTAB_READ_LATENCY+1]),
    .S_axis_tru_tvalid(S_axis_tru.tvalid),
    .S_axis_tru_tdata(S_axis_tru.tdata),
    .M_axis_tvalid(axis_decision_maker_tvalid),
    .M_axis_tdata(axis_decision_maker_tdata),
    .M_axis_tdest(axis_decision_maker_tdest),
    .M_axis_learning_fifo_tvalid(axis_learning_fifo_tvalid),
    .M_axis_learning_fifo_tdata(axis_learning_fifo_tdata),
    .dbg_mtab_unmatched(dbg_mtab_unmatched)

);

//////////////////////////////////////////////////////////////////////////////////
// forwarding engine output switch

axis_switch_1x2_40b_tdest5b output_switch_1x2_inst (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid(axis_decision_maker_tvalid),
  .s_axis_tready(axis_decision_maker_tready),
  .s_axis_tdata(axis_decision_maker_tdata),
  .s_axis_tdest(axis_decision_maker_tdest),
  .m_axis_tvalid({axis_output_switch_tvalid[1], axis_output_switch_tvalid[0]}),
  .m_axis_tready({axis_output_switch_tready[1], axis_output_switch_tready[0]}),
  .m_axis_tdata({axis_output_switch_tdata[1], axis_output_switch_tdata[0]}),
  .m_axis_tdest({axis_output_switch_tdest[1], axis_output_switch_tdest[0]}),
  .s_decode_err()
);

axis_switch_1x12_40b_tdest5b_0_11 output_switch_1x12_inst0 (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid(axis_output_switch_tvalid[0]),
  .s_axis_tready(axis_output_switch_tready[0]),
  .s_axis_tdata(axis_output_switch_tdata[0]),
  .s_axis_tdest(axis_output_switch_tdest[0]),
  .m_axis_tvalid({>>{axis_output_reg_slice_tvalid[11:0]}}),
  .m_axis_tready({>>{axis_output_reg_slice_tready[11:0]}}),
  .m_axis_tdata({>>{axis_output_reg_slice_tdata[11:0]}}),
  .m_axis_tdest(),
  .s_decode_err()
);

axis_switch_1x12_40b_tdest5b_12_23 output_switch_1x12_inst1 (
  .aclk(Clk),
  .aresetn(~Rst),
  .s_axis_tvalid(axis_output_switch_tvalid[1]),
  .s_axis_tready(axis_output_switch_tready[1]),
  .s_axis_tdata(axis_output_switch_tdata[1]),
  .s_axis_tdest(axis_output_switch_tdest[1]),
  .m_axis_tvalid({>>{axis_output_reg_slice_tvalid[23:12]}}),
  .m_axis_tready({>>{axis_output_reg_slice_tready[23:12]}}),
  .m_axis_tdata({>>{axis_output_reg_slice_tdata[23:12]}}),
  .m_axis_tdest(),
  .s_decode_err()
);

//////////////////////////////////////////////////////////////////////////////////
// forwarding engine output reg slices

generate
begin : output_reg_slices
for (genvar i = 0; i < `PORTS_NUM_PER_ENGINE; i++) begin : output_reg_slice
    axis_register_slice_40b output_reg_slice_inst (
      .aclk(Clk),
      .aresetn(~Rst),
      .s_axis_tvalid(axis_output_reg_slice_tvalid[i]),
      .s_axis_tready(axis_output_reg_slice_tready[i]),
      .s_axis_tdata(axis_output_reg_slice_tdata[i]),
      .m_axis_tvalid(M_axis[i].tvalid),
      .m_axis_tready(M_axis[i].tready), 
      .m_axis_tdata(M_axis[i].tdata)
    );
end
end
endgenerate

//////////////////////////////////////////////////////////////////////////////////
// forwarding engine learning fifo
    
xpm_fifo_axis #(
    .FIFO_DEPTH(`LEARNING_FIFO_DEPTH),
    .FIFO_MEMORY_TYPE("distributed"),
    .SIM_ASSERT_CHK(0),
    .TDATA_WIDTH(`LEARNING_FIFO_DATA_WIDTH),
    .USE_ADV_FEATURES("1000") // almost_full flag only
)
input_fifo_inst (
    .almost_empty_axis(),
    .almost_full_axis(dbg_learning_fifo_almost_full),
    .dbiterr_axis(),
    .m_axis_tdata(m_axis_learning_fifo_tdata),
    .m_axis_tdest(),
    .m_axis_tid(),
    .m_axis_tkeep(),
    .m_axis_tlast(),
    .m_axis_tstrb(),
    .m_axis_tuser(),
    .m_axis_tvalid(M_axis_learning_fifo.tvalid),
    .prog_empty_axis(),
    .prog_full_axis(),
    .rd_data_count_axis(),
    .s_axis_tready(axis_learning_fifo_tready),
    .sbiterr_axis(),
    .wr_data_count_axis(),
    .injectdbiterr_axis('0),
    .injectsbiterr_axis('0),
    .m_aclk(Clk),
    .m_axis_tready(M_axis_learning_fifo.tready),
    .s_aclk(Clk),
    .s_aresetn(~Rst),
    .s_axis_tdata(axis_learning_fifo_tdata),
    .s_axis_tdest('0),
    .s_axis_tid('0),
    .s_axis_tkeep('0),
    .s_axis_tlast('0),
    .s_axis_tstrb('0),
    .s_axis_tuser('0),
    .s_axis_tvalid(axis_learning_fifo_tvalid)
);

assign M_axis_learning_fifo.tdata = m_axis_learning_fifo_tdata[$size(t_learning_fifo_entry)-1:0];
   

endmodule
