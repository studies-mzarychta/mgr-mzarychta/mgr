package forwarding_engine_pkg;

`define DEBUG

`define CLK_PERIOD 16

`define MAX_PORTS_NUM         24 // maximum switch ports number
`define PORTS_NUM             24 // configured switch ports number, maximum MAX_PORTS_NUM
`define PORTS_NUM_PER_ENGINE  24 // number of ports assigned to single forwarding engine

`define MAC_WIDTH       48
`define FID_WIDTH        8
`define VID_WIDTH       12
`define PRIORITY_WIDTH   3
`define PORT_ID_WIDTH    $clog2(`MAX_PORTS_NUM)
`define SELECTOR_WIDTH   (`FID_WIDTH+`MAC_WIDTH)

`define FE_QUERY_ID_WIDTH           8

`define LEARNING_FIFO_DEPTH        16 // range: 16 - 4194304, must be power of two 
`define LEARNING_FIFO_DATA_WIDTH   72 // range: 8-2048, must be multiples of 8 and > $size(t_learning_fifo_entry)

`define VTAB_DEPTH          4096
`define VTAB_READ_LATENCY   2                   // range: 1-2, the number of register stages in the read data pipeline
                                                // change requires adjusting delay introduced by axis_register_slice_*_4cycles IP cores
`define VTAB_DATA_WIDTH     40                  // range: 8-2048, must be multiples of 8 and > $size(t_vtab_entry)
`define VTAB_ADDR_WIDTH     $clog2(`VTAB_DEPTH) 

`define HTAB_SUBTABLES_NUM  5
`define HTAB_DEPTH          4096
`define HTAB_READ_LATENCY   2                   // range: 1-2, the number of register stages in the read data pipeline
                                                // change requires adjusting delay introduced by axis_register_slice_*_4cycles IP cores
`define HTAB_DATA_WIDTH     72                  // range: 8-2048, must be multiples of 8 and > $size(t_htab_entry)
`define HTAB_ADDR_WIDTH     $clog2(`HTAB_DEPTH) 

`define MTAB_DEPTH          10240
`define MTAB_READ_LATENCY   2                   // range: 1-2, the number of register stages in the read data pipeline
                                                // change requires adjusting delay introduced by axis_register_slice_*_4cycles IP cores
`define MTAB_DATA_WIDTH     120                 // range: 8-2048, must be multiples of 8 and > $size(t_mtab_entry)
`define MTAB_ADDR_WIDTH     $clog2(`MTAB_DEPTH) 

// forwarding engine request data set 
typedef struct packed {
    logic [`FE_QUERY_ID_WIDTH-1:0] id;
    logic         [`MAC_WIDTH-1:0] smac;
    logic         [`MAC_WIDTH-1:0] dmac;
    logic         [`VID_WIDTH-1:0] vid;
    logic                          has_vid;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic                          has_prio;
    logic                    [6:0] rsvd;  // for future use
} t_forwarding_engine_request;

// forwarding engine response data set 
typedef struct packed {
    logic [`FE_QUERY_ID_WIDTH-1:0] id;
    logic         [`PORTS_NUM-1:0] port_mask;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic                          drop;
    logic                          hp;    // for future use
    logic                    [2:0] rsvd;  // for future use
} t_forwarding_engine_response;

// TRU module request data set
typedef struct packed {
    logic         [`MAC_WIDTH-1:0] smac;
    logic         [`MAC_WIDTH-1:0] dmac;
    logic         [`FID_WIDTH-1:0] fid;
    logic                          isHP;
    logic                          isBR;
    logic         [`PORTS_NUM-1:0] reqMask;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic                    [2:0] rsvd;  // for future use
} t_tru_request;

// TRU module response data set
typedef struct packed {
    logic         [`PORTS_NUM-1:0] port_mask;
    logic                          drop;
    logic         [`PORTS_NUM-1:0] respMask;
    logic                    [6:0] rsvd;  // for future use
} t_tru_response;

// learning fifo entry data set
typedef struct packed {
    logic     [`PORT_ID_WIDTH-1:0] pid;
    logic         [`MAC_WIDTH-1:0] smac;
    logic         [`VID_WIDTH-1:0] vid;
    logic                          has_vid;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic                          has_prio;
    logic                    [1:0] rsvd;  // for future use
} t_learning_fifo_entry;

// VTAB entry data set
typedef struct packed {
    logic                          drop;
    logic                          prio_override;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic                          has_prio;
    logic         [`FID_WIDTH-1:0] fid;
    logic     [`MAX_PORTS_NUM-1:0] port_mask;
    logic                    [1:0] rsvd;  // for future use
} t_vtab_entry;

// HTAB entry data set
typedef struct packed {
    logic         [`FID_WIDTH-1:0] fid;
    logic         [`MAC_WIDTH-1:0] mac;
    logic   [`MTAB_ADDR_WIDTH-1:0] ptr;
    logic                    [1:0] rsvd;  // for future use
} t_htab_entry;

// MTAB entry data set
typedef struct packed {
    logic         [`FID_WIDTH-1:0] fid;
    logic         [`MAC_WIDTH-1:0] mac;
    logic     [`MAX_PORTS_NUM-1:0] port_mask_src;
    logic     [`MAX_PORTS_NUM-1:0] port_mask_dst;
    logic                          drop_when_src;
    logic                          drop_when_dst;
    logic                          drop_unmatched_src;
    logic                          has_prio_src;
    logic                          prio_override_src;
    logic    [`PRIORITY_WIDTH-1:0] prio_src;
    logic                          has_prio_dst;
    logic                          prio_override_dst;
    logic    [`PRIORITY_WIDTH-1:0] prio_dst;
    logic                          is_link_local;
    logic                    [1:0] rsvd;  // for future use
} t_mtab_entry;


typedef struct packed {
    logic         [`FID_WIDTH-1:0] fid;
    logic         [`MAC_WIDTH-1:0] mac;
} t_htab_info;

typedef struct packed {
    logic   [`FE_QUERY_ID_WIDTH-1:0] id;
    logic       [`PORT_ID_WIDTH-1:0] pid;
    logic      [`PRIORITY_WIDTH-1:0] prio;
    logic                            has_prio;
    logic                      [6:0] rsvd;  // for future use
} t_forwarding_engine_request_decision_info;

typedef struct packed {
    logic                          drop;
    logic                          has_prio;
    logic                          prio_override;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic         [`PORTS_NUM-1:0] port_mask;
    logic                    [1:0] rsvd;  // for future use
} t_vtab_decision_info;

typedef struct packed {
    logic                          found;
    logic                          mtab_unmatched;
    logic         [`PORTS_NUM-1:0] port_mask;
    logic                          drop;
    logic                          drop_unmatched_src;
    logic                          has_prio;
    logic                          prio_override;
    logic    [`PRIORITY_WIDTH-1:0] prio;
    logic                          is_link_local;
    logic                    [5:0] rsvd;  // for future use
} t_mtab_decision_info;

 // convert one hot port mask to port id 
function [`PORT_ID_WIDTH-1:0] port_mask_to_pid (input logic [`PORTS_NUM-1:0] port_mask);
    automatic logic [`PORT_ID_WIDTH-1:0] pid = 0;
    foreach (port_mask[i])
    begin
        if (port_mask[i] == 1'b1)
            pid = pid | i ;
    end
    return pid;
endfunction

// convert port id to one hot port mask
function [`PORTS_NUM-1:0] pid_to_port_mask (input logic [`PORT_ID_WIDTH-1:0] pid);
    automatic logic [`PORTS_NUM-1:0] port_mask = {{{`PORTS_NUM-1}{1'b0}}, 1'b1};
    port_mask =  port_mask << pid;
    return port_mask;
endfunction

// reverse bits in 32b register
function [31:0] reverse32b;
input [31:0] data;
begin
    for (integer i=0; i<32; i=i+1)
        reverse32b[i] = data[31-i];
end
endfunction

// reverse bits in 16b register
function [15:0] reverse16b;
input [15:0] data;
begin
    for (integer i=0; i<16; i=i+1)
        reverse16b[i] = data[15-i];
end
endfunction

function [7:0] reverse8b;
input [7:0] data;
begin
    for (integer i=0; i<8; i=i+1)
        reverse8b[i] = data[7-i];
end
endfunction

function [55:0] reverse_bits_in_bytes_D56;
input [55:0] data;
begin
    reverse_bits_in_bytes_D56 = {reverse8b(data[55:48]), reverse8b(data[47:40]), reverse8b(data[39:32]), reverse8b(data[31:24]), reverse8b(data[23:16]), reverse8b(data[15:8]), reverse8b(data[7:0])};
end
endfunction
  
// polynomial: x^16 + x^12 + x^5 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [15:0] nextCRC16_CCITT_D56;

    input [55:0] Data;
    input [15:0] crc;
    reg [55:0] d;
    reg [15:0] c;
    reg [15:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[55] ^ d[52] ^ d[51] ^ d[49] ^ d[48] ^ d[42] ^ d[35] ^ d[33] ^ d[32] ^ d[28] ^ d[27] ^ d[26] ^ d[22] ^ d[20] ^ d[19] ^ d[12] ^ d[11] ^ d[8] ^ d[4] ^ d[0] ^ c[2] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[15];
    newcrc[1] = d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[43] ^ d[36] ^ d[34] ^ d[33] ^ d[29] ^ d[28] ^ d[27] ^ d[23] ^ d[21] ^ d[20] ^ d[13] ^ d[12] ^ d[9] ^ d[5] ^ d[1] ^ c[3] ^ c[9] ^ c[10] ^ c[12] ^ c[13];
    newcrc[2] = d[54] ^ d[53] ^ d[51] ^ d[50] ^ d[44] ^ d[37] ^ d[35] ^ d[34] ^ d[30] ^ d[29] ^ d[28] ^ d[24] ^ d[22] ^ d[21] ^ d[14] ^ d[13] ^ d[10] ^ d[6] ^ d[2] ^ c[4] ^ c[10] ^ c[11] ^ c[13] ^ c[14];
    newcrc[3] = d[55] ^ d[54] ^ d[52] ^ d[51] ^ d[45] ^ d[38] ^ d[36] ^ d[35] ^ d[31] ^ d[30] ^ d[29] ^ d[25] ^ d[23] ^ d[22] ^ d[15] ^ d[14] ^ d[11] ^ d[7] ^ d[3] ^ c[5] ^ c[11] ^ c[12] ^ c[14] ^ c[15];
    newcrc[4] = d[55] ^ d[53] ^ d[52] ^ d[46] ^ d[39] ^ d[37] ^ d[36] ^ d[32] ^ d[31] ^ d[30] ^ d[26] ^ d[24] ^ d[23] ^ d[16] ^ d[15] ^ d[12] ^ d[8] ^ d[4] ^ c[6] ^ c[12] ^ c[13] ^ c[15];
    newcrc[5] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[48] ^ d[47] ^ d[42] ^ d[40] ^ d[38] ^ d[37] ^ d[35] ^ d[31] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[22] ^ d[20] ^ d[19] ^ d[17] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[9] ^ d[8] ^ d[5] ^ d[4] ^ d[0] ^ c[0] ^ c[2] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[6] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[43] ^ d[41] ^ d[39] ^ d[38] ^ d[36] ^ d[32] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[23] ^ d[21] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[13] ^ d[12] ^ d[10] ^ d[9] ^ d[6] ^ d[5] ^ d[1] ^ c[1] ^ c[3] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[7] = d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[50] ^ d[49] ^ d[44] ^ d[42] ^ d[40] ^ d[39] ^ d[37] ^ d[33] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[24] ^ d[22] ^ d[21] ^ d[19] ^ d[18] ^ d[15] ^ d[14] ^ d[13] ^ d[11] ^ d[10] ^ d[7] ^ d[6] ^ d[2] ^ c[0] ^ c[2] ^ c[4] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[15];
    newcrc[8] = d[55] ^ d[54] ^ d[52] ^ d[51] ^ d[50] ^ d[45] ^ d[43] ^ d[41] ^ d[40] ^ d[38] ^ d[34] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[23] ^ d[22] ^ d[20] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[11] ^ d[8] ^ d[7] ^ d[3] ^ c[0] ^ c[1] ^ c[3] ^ c[5] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15];
    newcrc[9] = d[55] ^ d[53] ^ d[52] ^ d[51] ^ d[46] ^ d[44] ^ d[42] ^ d[41] ^ d[39] ^ d[35] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[24] ^ d[23] ^ d[21] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[12] ^ d[9] ^ d[8] ^ d[4] ^ c[1] ^ c[2] ^ c[4] ^ c[6] ^ c[11] ^ c[12] ^ c[13] ^ c[15];
    newcrc[10] = d[54] ^ d[53] ^ d[52] ^ d[47] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[25] ^ d[24] ^ d[22] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[13] ^ d[10] ^ d[9] ^ d[5] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[12] ^ c[13] ^ c[14];
    newcrc[11] = d[55] ^ d[54] ^ d[53] ^ d[48] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[37] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[26] ^ d[25] ^ d[23] ^ d[22] ^ d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[14] ^ d[11] ^ d[10] ^ d[6] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[8] ^ c[13] ^ c[14] ^ c[15];
    newcrc[12] = d[54] ^ d[52] ^ d[51] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[38] ^ d[31] ^ d[29] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[18] ^ d[16] ^ d[15] ^ d[8] ^ d[7] ^ d[4] ^ d[0] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[11] ^ c[12] ^ c[14];
    newcrc[13] = d[55] ^ d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[46] ^ d[45] ^ d[39] ^ d[32] ^ d[30] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[19] ^ d[17] ^ d[16] ^ d[9] ^ d[8] ^ d[5] ^ d[1] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[12] ^ c[13] ^ c[15];
    newcrc[14] = d[54] ^ d[53] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[40] ^ d[33] ^ d[31] ^ d[30] ^ d[26] ^ d[25] ^ d[24] ^ d[20] ^ d[18] ^ d[17] ^ d[10] ^ d[9] ^ d[6] ^ d[2] ^ c[0] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[13] ^ c[14];
    newcrc[15] = d[55] ^ d[54] ^ d[51] ^ d[50] ^ d[48] ^ d[47] ^ d[41] ^ d[34] ^ d[32] ^ d[31] ^ d[27] ^ d[26] ^ d[25] ^ d[21] ^ d[19] ^ d[18] ^ d[11] ^ d[10] ^ d[7] ^ d[3] ^ c[1] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[14] ^ c[15];
    nextCRC16_CCITT_D56 = newcrc;
end
endfunction

// polynomial: x^16 + x^15 + x^14 + x^11 + x^6 + x^5 + x^2 + x^1 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [15:0] nextCRC16_CDMA2000_D56;

    input [55:0] Data;
    input [15:0] crc;
    reg [55:0] d;
    reg [15:0] c;
    reg [15:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[55] ^ d[53] ^ d[52] ^ d[50] ^ d[47] ^ d[46] ^ d[44] ^ d[42] ^ d[38] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[30] ^ d[28] ^ d[27] ^ d[25] ^ d[22] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[10] ^ c[12] ^ c[13] ^ c[15];
    newcrc[1] = d[55] ^ d[54] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[45] ^ d[44] ^ d[43] ^ d[42] ^ d[39] ^ d[38] ^ d[37] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[23] ^ d[21] ^ d[19] ^ d[16] ^ d[14] ^ d[9] ^ d[7] ^ d[3] ^ d[2] ^ d[0] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15];
    newcrc[2] = d[51] ^ d[50] ^ d[49] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[39] ^ d[36] ^ d[35] ^ d[34] ^ d[31] ^ d[26] ^ d[25] ^ d[24] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[13] ^ d[12] ^ d[11] ^ d[9] ^ d[8] ^ d[6] ^ d[5] ^ d[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[9] ^ c[10] ^ c[11];
    newcrc[3] = d[52] ^ d[51] ^ d[50] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[40] ^ d[37] ^ d[36] ^ d[35] ^ d[32] ^ d[27] ^ d[26] ^ d[25] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[14] ^ d[13] ^ d[12] ^ d[10] ^ d[9] ^ d[7] ^ d[6] ^ d[1] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[10] ^ c[11] ^ c[12];
    newcrc[4] = d[53] ^ d[52] ^ d[51] ^ d[47] ^ d[45] ^ d[44] ^ d[42] ^ d[41] ^ d[38] ^ d[37] ^ d[36] ^ d[33] ^ d[28] ^ d[27] ^ d[26] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[11] ^ d[10] ^ d[8] ^ d[7] ^ d[2] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[11] ^ c[12] ^ c[13];
    newcrc[5] = d[55] ^ d[54] ^ d[50] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[43] ^ d[39] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[32] ^ d[30] ^ d[29] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[19] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[10] ^ d[8] ^ d[6] ^ d[5] ^ d[4] ^ d[1] ^ d[0] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[10] ^ c[14] ^ c[15];
    newcrc[6] = d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[42] ^ d[40] ^ d[37] ^ d[35] ^ d[32] ^ d[31] ^ d[28] ^ d[27] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[10] ^ d[7] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[0] ^ c[2] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13];
    newcrc[7] = d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[43] ^ d[41] ^ d[38] ^ d[36] ^ d[33] ^ d[32] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[11] ^ d[8] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ c[1] ^ c[3] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14];
    newcrc[8] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[44] ^ d[42] ^ d[39] ^ d[37] ^ d[34] ^ d[33] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ c[2] ^ c[4] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[9] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[45] ^ d[43] ^ d[40] ^ d[38] ^ d[35] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ c[0] ^ c[3] ^ c[5] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[10] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[46] ^ d[44] ^ d[41] ^ d[39] ^ d[36] ^ d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ c[1] ^ c[4] ^ c[6] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[11] = d[54] ^ d[46] ^ d[45] ^ d[44] ^ d[40] ^ d[38] ^ d[37] ^ d[35] ^ d[34] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[21] ^ d[20] ^ d[19] ^ d[16] ^ d[15] ^ d[13] ^ d[11] ^ d[10] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[4] ^ c[5] ^ c[6] ^ c[14];
    newcrc[12] = d[55] ^ d[47] ^ d[46] ^ d[45] ^ d[41] ^ d[39] ^ d[38] ^ d[36] ^ d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[27] ^ d[22] ^ d[21] ^ d[20] ^ d[17] ^ d[16] ^ d[14] ^ d[12] ^ d[11] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[1] ^ c[5] ^ c[6] ^ c[7] ^ c[15];
    newcrc[13] = d[48] ^ d[47] ^ d[46] ^ d[42] ^ d[40] ^ d[39] ^ d[37] ^ d[36] ^ d[33] ^ d[32] ^ d[31] ^ d[28] ^ d[23] ^ d[22] ^ d[21] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[12] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[5] ^ d[3] ^ d[2] ^ c[0] ^ c[2] ^ c[6] ^ c[7] ^ c[8];
    newcrc[14] = d[55] ^ d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[44] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[37] ^ d[36] ^ d[35] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[17] ^ d[14] ^ d[12] ^ d[7] ^ d[5] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[15];
    newcrc[15] = d[55] ^ d[54] ^ d[52] ^ d[51] ^ d[49] ^ d[46] ^ d[45] ^ d[43] ^ d[41] ^ d[37] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[29] ^ d[27] ^ d[26] ^ d[24] ^ d[21] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1] ^ c[3] ^ c[5] ^ c[6] ^ c[9] ^ c[11] ^ c[12] ^ c[14] ^ c[15];
    nextCRC16_CDMA2000_D56 = newcrc;
end
endfunction

// polynomial: x^16 + x^10 + x^8 + x^7 + x^3 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [15:0] nextCRC16_DECT_D56;

    input [55:0] Data;
    input [15:0] crc;
    reg [55:0] d;
    reg [15:0] c;
    reg [15:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[55] ^ d[52] ^ d[51] ^ d[49] ^ d[48] ^ d[46] ^ d[44] ^ d[39] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[24] ^ d[22] ^ d[21] ^ d[20] ^ d[13] ^ d[12] ^ d[9] ^ d[8] ^ d[6] ^ d[0] ^ c[4] ^ c[6] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[15];
    newcrc[1] = d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[47] ^ d[45] ^ d[40] ^ d[38] ^ d[36] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[25] ^ d[23] ^ d[22] ^ d[21] ^ d[14] ^ d[13] ^ d[10] ^ d[9] ^ d[7] ^ d[1] ^ c[0] ^ c[5] ^ c[7] ^ c[9] ^ c[10] ^ c[12] ^ c[13];
    newcrc[2] = d[54] ^ d[53] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[41] ^ d[39] ^ d[37] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[15] ^ d[14] ^ d[11] ^ d[10] ^ d[8] ^ d[2] ^ c[1] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[13] ^ c[14];
    newcrc[3] = d[54] ^ d[48] ^ d[47] ^ d[46] ^ d[44] ^ d[42] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[29] ^ d[28] ^ d[25] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[16] ^ d[15] ^ d[13] ^ d[11] ^ d[8] ^ d[6] ^ d[3] ^ d[0] ^ c[0] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[14];
    newcrc[4] = d[55] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[43] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[30] ^ d[29] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[17] ^ d[16] ^ d[14] ^ d[12] ^ d[9] ^ d[7] ^ d[4] ^ d[1] ^ c[0] ^ c[1] ^ c[3] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[15];
    newcrc[5] = d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[44] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[31] ^ d[30] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[10] ^ d[8] ^ d[5] ^ d[2] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[6] ^ c[8] ^ c[9] ^ c[10];
    newcrc[6] = d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[45] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[37] ^ d[32] ^ d[31] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[11] ^ d[9] ^ d[6] ^ d[3] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[9] ^ c[10] ^ c[11];
    newcrc[7] = d[55] ^ d[50] ^ d[49] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[35] ^ d[31] ^ d[30] ^ d[28] ^ d[26] ^ d[25] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[9] ^ c[10] ^ c[15];
    newcrc[8] = d[55] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[30] ^ d[28] ^ d[26] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[16] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[15];
    newcrc[9] = d[53] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[44] ^ d[43] ^ d[42] ^ d[41] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[31] ^ d[29] ^ d[27] ^ d[25] ^ d[24] ^ d[22] ^ d[19] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[2] ^ d[1] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[13];
    newcrc[10] = d[55] ^ d[54] ^ d[50] ^ d[49] ^ d[46] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[38] ^ d[33] ^ d[31] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[7] ^ d[6] ^ d[3] ^ d[2] ^ d[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[9] ^ c[10] ^ c[14] ^ c[15];
    newcrc[11] = d[55] ^ d[51] ^ d[50] ^ d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[39] ^ d[34] ^ d[32] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[8] ^ d[7] ^ d[4] ^ d[3] ^ d[1] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[10] ^ c[11] ^ c[15];
    newcrc[12] = d[52] ^ d[51] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[42] ^ d[40] ^ d[35] ^ d[33] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[9] ^ d[8] ^ d[5] ^ d[4] ^ d[2] ^ c[0] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[11] ^ c[12];
    newcrc[13] = d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[46] ^ d[45] ^ d[43] ^ d[41] ^ d[36] ^ d[34] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[10] ^ d[9] ^ d[6] ^ d[5] ^ d[3] ^ c[1] ^ c[3] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[12] ^ c[13];
    newcrc[14] = d[54] ^ d[53] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[44] ^ d[42] ^ d[37] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[22] ^ d[20] ^ d[19] ^ d[18] ^ d[11] ^ d[10] ^ d[7] ^ d[6] ^ d[4] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[13] ^ c[14];
    newcrc[15] = d[55] ^ d[54] ^ d[51] ^ d[50] ^ d[48] ^ d[47] ^ d[45] ^ d[43] ^ d[38] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[23] ^ d[21] ^ d[20] ^ d[19] ^ d[12] ^ d[11] ^ d[8] ^ d[7] ^ d[5] ^ c[3] ^ c[5] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[14] ^ c[15];
    nextCRC16_DECT_D56 = newcrc;
end
endfunction

// polynomial: x^16 + x^15 + x^2 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [15:0] nextCRC16_IBM_D56;

    input [55:0] Data;
    input [15:0] crc;
    reg [55:0] d;
    reg [15:0] c;
    reg [15:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[45] ^ d[43] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[3] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[1] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[44] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[2] = d[46] ^ d[42] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[16] ^ d[14] ^ d[1] ^ d[0] ^ c[2] ^ c[6];
    newcrc[3] = d[47] ^ d[43] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[17] ^ d[15] ^ d[2] ^ d[1] ^ c[3] ^ c[7];
    newcrc[4] = d[48] ^ d[44] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[18] ^ d[16] ^ d[3] ^ d[2] ^ c[4] ^ c[8];
    newcrc[5] = d[49] ^ d[45] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[19] ^ d[17] ^ d[4] ^ d[3] ^ c[5] ^ c[9];
    newcrc[6] = d[50] ^ d[46] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[20] ^ d[18] ^ d[5] ^ d[4] ^ c[6] ^ c[10];
    newcrc[7] = d[51] ^ d[47] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[21] ^ d[19] ^ d[6] ^ d[5] ^ c[7] ^ c[11];
    newcrc[8] = d[52] ^ d[48] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[22] ^ d[20] ^ d[7] ^ d[6] ^ c[8] ^ c[12];
    newcrc[9] = d[53] ^ d[49] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[23] ^ d[21] ^ d[8] ^ d[7] ^ c[9] ^ c[13];
    newcrc[10] = d[54] ^ d[50] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[24] ^ d[22] ^ d[9] ^ d[8] ^ c[10] ^ c[14];
    newcrc[11] = d[55] ^ d[51] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[25] ^ d[23] ^ d[10] ^ d[9] ^ c[0] ^ c[11] ^ c[15];
    newcrc[12] = d[52] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[26] ^ d[24] ^ d[11] ^ d[10] ^ c[0] ^ c[1] ^ c[12];
    newcrc[13] = d[53] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[27] ^ d[25] ^ d[12] ^ d[11] ^ c[0] ^ c[1] ^ c[2] ^ c[13];
    newcrc[14] = d[54] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[28] ^ d[26] ^ d[13] ^ d[12] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[14];
    newcrc[15] = d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[45] ^ d[44] ^ d[42] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14];
    nextCRC16_IBM_D56 = newcrc;
end
endfunction

// polynomial: x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x^1 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [31:0] nextCRC32_D56;

    input [55:0] Data;
    input [31:0] crc;
    reg [55:0] d;
    reg [31:0] c;
    reg [31:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[55] ^ d[54] ^ d[53] ^ d[50] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[37] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[16] ^ d[12] ^ d[10] ^ d[9] ^ d[6] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[10] ^ c[13] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[29] ^ c[30] ^ c[31];
    newcrc[1] = d[53] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[44] ^ d[38] ^ d[37] ^ d[35] ^ d[34] ^ d[33] ^ d[28] ^ d[27] ^ d[24] ^ d[17] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[9] ^ d[7] ^ d[6] ^ d[1] ^ d[0] ^ c[0] ^ c[3] ^ c[4] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[29];
    newcrc[2] = d[55] ^ d[53] ^ d[52] ^ d[51] ^ d[44] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[26] ^ d[24] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[13] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[6] ^ c[7] ^ c[8] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[20] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
    newcrc[3] = d[54] ^ d[53] ^ d[52] ^ d[45] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[33] ^ d[32] ^ d[31] ^ d[27] ^ d[25] ^ d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[14] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[3] ^ d[2] ^ d[1] ^ c[1] ^ c[3] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[21] ^ c[28] ^ c[29] ^ c[30];
    newcrc[4] = d[50] ^ d[48] ^ d[47] ^ d[46] ^ d[45] ^ d[44] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[25] ^ d[24] ^ d[20] ^ d[19] ^ d[18] ^ d[15] ^ d[12] ^ d[11] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[0] ^ c[1] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[26];
    newcrc[5] = d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[50] ^ d[49] ^ d[46] ^ d[44] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[37] ^ d[29] ^ d[28] ^ d[24] ^ d[21] ^ d[20] ^ d[19] ^ d[13] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[4] ^ c[5] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[20] ^ c[22] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
    newcrc[6] = d[55] ^ d[54] ^ d[52] ^ d[51] ^ d[50] ^ d[47] ^ d[45] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[38] ^ d[30] ^ d[29] ^ d[25] ^ d[22] ^ d[21] ^ d[20] ^ d[14] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[1] ^ c[5] ^ c[6] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
    newcrc[7] = d[54] ^ d[52] ^ d[51] ^ d[50] ^ d[47] ^ d[46] ^ d[45] ^ d[43] ^ d[42] ^ d[41] ^ d[39] ^ d[37] ^ d[34] ^ d[32] ^ d[29] ^ d[28] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[16] ^ d[15] ^ d[10] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[0] ^ c[1] ^ c[4] ^ c[5] ^ c[8] ^ c[10] ^ c[13] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
    newcrc[8] = d[54] ^ d[52] ^ d[51] ^ d[50] ^ d[46] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[38] ^ d[37] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[28] ^ d[23] ^ d[22] ^ d[17] ^ d[12] ^ d[11] ^ d[10] ^ d[8] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[4] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[14] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
    newcrc[9] = d[55] ^ d[53] ^ d[52] ^ d[51] ^ d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[39] ^ d[38] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[29] ^ d[24] ^ d[23] ^ d[18] ^ d[13] ^ d[12] ^ d[11] ^ d[9] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[5] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
    newcrc[10] = d[55] ^ d[52] ^ d[50] ^ d[42] ^ d[40] ^ d[39] ^ d[36] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[26] ^ d[19] ^ d[16] ^ d[14] ^ d[13] ^ d[9] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[15] ^ c[16] ^ c[18] ^ c[26] ^ c[28] ^ c[31];
    newcrc[11] = d[55] ^ d[54] ^ d[51] ^ d[50] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[43] ^ d[41] ^ d[40] ^ d[36] ^ d[33] ^ d[31] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[9] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[7] ^ c[9] ^ c[12] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[30] ^ c[31];
    newcrc[12] = d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[42] ^ d[41] ^ d[31] ^ d[30] ^ d[27] ^ d[24] ^ d[21] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[12] ^ d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[3] ^ c[6] ^ c[7] ^ c[17] ^ c[18] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
    newcrc[13] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[47] ^ d[43] ^ d[42] ^ d[32] ^ d[31] ^ d[28] ^ d[25] ^ d[22] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[13] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ c[1] ^ c[4] ^ c[7] ^ c[8] ^ c[18] ^ c[19] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
    newcrc[14] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[48] ^ d[44] ^ d[43] ^ d[33] ^ d[32] ^ d[29] ^ d[26] ^ d[23] ^ d[20] ^ d[19] ^ d[17] ^ d[15] ^ d[14] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ c[2] ^ c[5] ^ c[8] ^ c[9] ^ c[19] ^ c[20] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
    newcrc[15] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[45] ^ d[44] ^ d[34] ^ d[33] ^ d[30] ^ d[27] ^ d[24] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[4] ^ d[3] ^ c[0] ^ c[3] ^ c[6] ^ c[9] ^ c[10] ^ c[20] ^ c[21] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
    newcrc[16] = d[51] ^ d[48] ^ d[47] ^ d[46] ^ d[44] ^ d[37] ^ d[35] ^ d[32] ^ d[30] ^ d[29] ^ d[26] ^ d[24] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[13] ^ d[12] ^ d[8] ^ d[5] ^ d[4] ^ d[0] ^ c[0] ^ c[2] ^ c[5] ^ c[6] ^ c[8] ^ c[11] ^ c[13] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[27];
    newcrc[17] = d[52] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[38] ^ d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[27] ^ d[25] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[14] ^ d[13] ^ d[9] ^ d[6] ^ d[5] ^ d[1] ^ c[1] ^ c[3] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[14] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[28];
    newcrc[18] = d[53] ^ d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[39] ^ d[37] ^ d[34] ^ d[32] ^ d[31] ^ d[28] ^ d[26] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[15] ^ d[14] ^ d[10] ^ d[7] ^ d[6] ^ d[2] ^ c[0] ^ c[2] ^ c[4] ^ c[7] ^ c[8] ^ c[10] ^ c[13] ^ c[15] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[29];
    newcrc[19] = d[54] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[40] ^ d[38] ^ d[35] ^ d[33] ^ d[32] ^ d[29] ^ d[27] ^ d[25] ^ d[24] ^ d[22] ^ d[20] ^ d[16] ^ d[15] ^ d[11] ^ d[8] ^ d[7] ^ d[3] ^ c[0] ^ c[1] ^ c[3] ^ c[5] ^ c[8] ^ c[9] ^ c[11] ^ c[14] ^ c[16] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[30];
    newcrc[20] = d[55] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[41] ^ d[39] ^ d[36] ^ d[34] ^ d[33] ^ d[30] ^ d[28] ^ d[26] ^ d[25] ^ d[23] ^ d[21] ^ d[17] ^ d[16] ^ d[12] ^ d[9] ^ d[8] ^ d[4] ^ c[1] ^ c[2] ^ c[4] ^ c[6] ^ c[9] ^ c[10] ^ c[12] ^ c[15] ^ c[17] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
    newcrc[21] = d[53] ^ d[52] ^ d[51] ^ d[49] ^ d[42] ^ d[40] ^ d[37] ^ d[35] ^ d[34] ^ d[31] ^ d[29] ^ d[27] ^ d[26] ^ d[24] ^ d[22] ^ d[18] ^ d[17] ^ d[13] ^ d[10] ^ d[9] ^ d[5] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[10] ^ c[11] ^ c[13] ^ c[16] ^ c[18] ^ c[25] ^ c[27] ^ c[28] ^ c[29];
    newcrc[22] = d[55] ^ d[52] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[43] ^ d[41] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[31] ^ d[29] ^ d[27] ^ d[26] ^ d[24] ^ d[23] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[12] ^ d[11] ^ d[9] ^ d[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[28] ^ c[31];
    newcrc[23] = d[55] ^ d[54] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[42] ^ d[39] ^ d[38] ^ d[36] ^ d[35] ^ d[34] ^ d[31] ^ d[29] ^ d[27] ^ d[26] ^ d[20] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[9] ^ d[6] ^ d[1] ^ d[0] ^ c[2] ^ c[3] ^ c[5] ^ c[7] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[18] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
    newcrc[24] = d[55] ^ d[51] ^ d[50] ^ d[48] ^ d[47] ^ d[43] ^ d[40] ^ d[39] ^ d[37] ^ d[36] ^ d[35] ^ d[32] ^ d[30] ^ d[28] ^ d[27] ^ d[21] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[10] ^ d[7] ^ d[2] ^ d[1] ^ c[3] ^ c[4] ^ c[6] ^ c[8] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[19] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[31];
    newcrc[25] = d[52] ^ d[51] ^ d[49] ^ d[48] ^ d[44] ^ d[41] ^ d[40] ^ d[38] ^ d[37] ^ d[36] ^ d[33] ^ d[31] ^ d[29] ^ d[28] ^ d[22] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[11] ^ d[8] ^ d[3] ^ d[2] ^ c[4] ^ c[5] ^ c[7] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[16] ^ c[17] ^ c[20] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
    newcrc[26] = d[55] ^ d[54] ^ d[52] ^ d[49] ^ d[48] ^ d[47] ^ d[44] ^ d[42] ^ d[41] ^ d[39] ^ d[38] ^ d[31] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[19] ^ d[18] ^ d[10] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[7] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[20] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[30] ^ c[31];
    newcrc[27] = d[55] ^ d[53] ^ d[50] ^ d[49] ^ d[48] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[39] ^ d[32] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[20] ^ d[19] ^ d[11] ^ d[7] ^ d[5] ^ d[4] ^ d[1] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[8] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[25] ^ c[26] ^ c[29] ^ c[31];
    newcrc[28] = d[54] ^ d[51] ^ d[50] ^ d[49] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[40] ^ d[33] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[22] ^ d[21] ^ d[20] ^ d[12] ^ d[8] ^ d[6] ^ d[5] ^ d[2] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[9] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[25] ^ c[26] ^ c[27] ^ c[30];
    newcrc[29] = d[55] ^ d[52] ^ d[51] ^ d[50] ^ d[47] ^ d[45] ^ d[44] ^ d[42] ^ d[41] ^ d[34] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[23] ^ d[22] ^ d[21] ^ d[13] ^ d[9] ^ d[7] ^ d[6] ^ d[3] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[10] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[23] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
    newcrc[30] = d[53] ^ d[52] ^ d[51] ^ d[48] ^ d[46] ^ d[45] ^ d[43] ^ d[42] ^ d[35] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[14] ^ d[10] ^ d[8] ^ d[7] ^ d[4] ^ c[0] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[11] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[24] ^ c[27] ^ c[28] ^ c[29];
    newcrc[31] = d[54] ^ d[53] ^ d[52] ^ d[49] ^ d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[15] ^ d[11] ^ d[9] ^ d[8] ^ d[5] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[25] ^ c[28] ^ c[29] ^ c[30];
    nextCRC32_D56 = newcrc;
end
endfunction

// polynomial: x^32 + x^28 + x^27 + x^26 + x^25 + x^23 + x^22 + x^20 + x^19 + x^18 + x^14 + x^13 + x^11 + x^10 + x^9 + x^8 + x^6 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [31:0] nextCRC32_C_D56;

    input [55:0] Data;
    input [31:0] crc;
    reg [55:0] d;
    reg [31:0] c;
    reg [31:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[54] ^ d[53] ^ d[51] ^ d[48] ^ d[47] ^ d[46] ^ d[45] ^ d[43] ^ d[42] ^ d[37] ^ d[36] ^ d[35] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[12] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[11] ^ c[12] ^ c[13] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[27] ^ c[29] ^ c[30];
    newcrc[1] = d[55] ^ d[54] ^ d[52] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[38] ^ d[37] ^ d[36] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[26] ^ d[24] ^ d[22] ^ d[19] ^ d[18] ^ d[17] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[1] ^ c[0] ^ c[2] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[12] ^ c[13] ^ c[14] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[30] ^ c[31];
    newcrc[2] = d[55] ^ d[53] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[39] ^ d[38] ^ d[37] ^ d[33] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[23] ^ d[20] ^ d[19] ^ d[18] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[2] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[13] ^ c[14] ^ c[15] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[29] ^ c[31];
    newcrc[3] = d[54] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[45] ^ d[40] ^ d[39] ^ d[38] ^ d[34] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[24] ^ d[21] ^ d[20] ^ d[19] ^ d[15] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[3] ^ c[0] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[14] ^ c[15] ^ c[16] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[30];
    newcrc[4] = d[55] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[41] ^ d[40] ^ d[39] ^ d[35] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[25] ^ d[22] ^ d[21] ^ d[20] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[4] ^ c[1] ^ c[3] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[15] ^ c[16] ^ c[17] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
    newcrc[5] = d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[47] ^ d[42] ^ d[41] ^ d[40] ^ d[36] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[21] ^ d[17] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[5] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[16] ^ c[17] ^ c[18] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[29];
    newcrc[6] = d[52] ^ d[49] ^ d[47] ^ d[46] ^ d[45] ^ d[41] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[22] ^ d[21] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[4] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[17] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[28];
    newcrc[7] = d[53] ^ d[50] ^ d[48] ^ d[47] ^ d[46] ^ d[42] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[23] ^ d[22] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[5] ^ d[1] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[18] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[29];
    newcrc[8] = d[53] ^ d[49] ^ d[46] ^ d[45] ^ d[42] ^ d[34] ^ d[32] ^ d[25] ^ d[24] ^ d[21] ^ d[19] ^ d[15] ^ d[13] ^ d[11] ^ d[10] ^ d[8] ^ d[5] ^ d[4] ^ d[2] ^ d[0] ^ c[0] ^ c[1] ^ c[8] ^ c[10] ^ c[18] ^ c[21] ^ c[22] ^ c[25] ^ c[29];
    newcrc[9] = d[53] ^ d[51] ^ d[50] ^ d[48] ^ d[45] ^ d[42] ^ d[37] ^ d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[17] ^ d[14] ^ d[11] ^ d[8] ^ d[7] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[13] ^ c[18] ^ c[21] ^ c[24] ^ c[26] ^ c[27] ^ c[29];
    newcrc[10] = d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[42] ^ d[38] ^ d[36] ^ d[35] ^ d[34] ^ d[32] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[22] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[7] ^ d[6] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[18] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[28] ^ c[29];
    newcrc[11] = d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[45] ^ d[42] ^ d[39] ^ d[33] ^ d[21] ^ d[20] ^ d[12] ^ d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[9] ^ c[15] ^ c[18] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[27];
    newcrc[12] = d[52] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[43] ^ d[40] ^ d[34] ^ d[22] ^ d[21] ^ d[13] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[10] ^ c[16] ^ c[19] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[28];
    newcrc[13] = d[54] ^ d[52] ^ d[49] ^ d[48] ^ d[46] ^ d[45] ^ d[44] ^ d[43] ^ d[42] ^ d[41] ^ d[37] ^ d[36] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[25] ^ d[22] ^ d[21] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[12] ^ d[11] ^ d[9] ^ d[3] ^ d[2] ^ d[0] ^ c[1] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[12] ^ c[13] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[28] ^ c[30];
    newcrc[14] = d[55] ^ d[54] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[44] ^ d[38] ^ d[36] ^ d[35] ^ d[32] ^ d[30] ^ d[29] ^ d[25] ^ d[22] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[13] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[1] ^ c[5] ^ c[6] ^ c[8] ^ c[11] ^ c[12] ^ c[14] ^ c[20] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[30] ^ c[31];
    newcrc[15] = d[55] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[45] ^ d[39] ^ d[37] ^ d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[26] ^ d[23] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[14] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[2] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[13] ^ c[15] ^ c[21] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
    newcrc[16] = d[53] ^ d[52] ^ d[51] ^ d[50] ^ d[46] ^ d[40] ^ d[38] ^ d[37] ^ d[34] ^ d[32] ^ d[31] ^ d[27] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[15] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ c[0] ^ c[3] ^ c[7] ^ c[8] ^ c[10] ^ c[13] ^ c[14] ^ c[16] ^ c[22] ^ c[26] ^ c[27] ^ c[28] ^ c[29];
    newcrc[17] = d[54] ^ d[53] ^ d[52] ^ d[51] ^ d[47] ^ d[41] ^ d[39] ^ d[38] ^ d[35] ^ d[33] ^ d[32] ^ d[28] ^ d[25] ^ d[24] ^ d[22] ^ d[19] ^ d[18] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ c[0] ^ c[1] ^ c[4] ^ c[8] ^ c[9] ^ c[11] ^ c[14] ^ c[15] ^ c[17] ^ c[23] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
    newcrc[18] = d[55] ^ d[52] ^ d[51] ^ d[47] ^ d[46] ^ d[45] ^ d[43] ^ d[40] ^ d[39] ^ d[37] ^ d[35] ^ d[34] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[13] ^ d[11] ^ d[10] ^ d[8] ^ d[6] ^ d[0] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[13] ^ c[15] ^ c[16] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[27] ^ c[28] ^ c[31];
    newcrc[19] = d[54] ^ d[52] ^ d[51] ^ d[45] ^ d[44] ^ d[43] ^ d[42] ^ d[41] ^ d[40] ^ d[38] ^ d[37] ^ d[34] ^ d[32] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[23] ^ d[22] ^ d[20] ^ d[19] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[11] ^ d[8] ^ d[6] ^ d[5] ^ d[4] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[8] ^ c[10] ^ c[13] ^ c[14] ^ c[16] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[27] ^ c[28] ^ c[30];
    newcrc[20] = d[55] ^ d[54] ^ d[52] ^ d[51] ^ d[48] ^ d[47] ^ d[44] ^ d[41] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[33] ^ d[31] ^ d[25] ^ d[24] ^ d[20] ^ d[19] ^ d[18] ^ d[15] ^ d[8] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[7] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[17] ^ c[20] ^ c[23] ^ c[24] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
    newcrc[21] = d[55] ^ d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[45] ^ d[42] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[34] ^ d[32] ^ d[26] ^ d[25] ^ d[21] ^ d[20] ^ d[19] ^ d[16] ^ d[9] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ c[1] ^ c[2] ^ c[8] ^ c[10] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[18] ^ c[21] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
    newcrc[22] = d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[33] ^ d[31] ^ d[30] ^ d[28] ^ d[25] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[16] ^ d[12] ^ d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[1] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[27];
    newcrc[23] = d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[47] ^ d[45] ^ d[41] ^ d[40] ^ d[39] ^ d[38] ^ d[36] ^ d[35] ^ d[34] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[19] ^ d[18] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[7] ^ d[5] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[14] ^ c[15] ^ c[16] ^ c[17] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
    newcrc[24] = d[55] ^ d[54] ^ d[53] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[42] ^ d[41] ^ d[40] ^ d[39] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[20] ^ d[19] ^ d[17] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[8] ^ d[6] ^ d[4] ^ d[2] ^ d[1] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[17] ^ c[18] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
    newcrc[25] = d[55] ^ d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[46] ^ d[45] ^ d[41] ^ d[40] ^ d[38] ^ d[35] ^ d[34] ^ d[32] ^ d[29] ^ d[28] ^ d[25] ^ d[23] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[1] ^ c[4] ^ c[5] ^ c[8] ^ c[10] ^ c[11] ^ c[14] ^ c[16] ^ c[17] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
    newcrc[26] = d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[45] ^ d[43] ^ d[41] ^ d[39] ^ d[37] ^ d[33] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[15] ^ d[14] ^ d[12] ^ d[8] ^ d[6] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[9] ^ c[13] ^ c[15] ^ c[17] ^ c[19] ^ c[21] ^ c[24] ^ c[25] ^ c[26] ^ c[27];
    newcrc[27] = d[54] ^ d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[43] ^ d[40] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[32] ^ d[31] ^ d[29] ^ d[27] ^ d[24] ^ d[23] ^ d[21] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[12] ^ d[8] ^ d[6] ^ d[5] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[3] ^ c[5] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[16] ^ c[19] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
    newcrc[28] = d[55] ^ d[50] ^ d[49] ^ d[47] ^ d[44] ^ d[43] ^ d[42] ^ d[41] ^ d[39] ^ d[38] ^ d[33] ^ d[32] ^ d[31] ^ d[27] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[14] ^ d[13] ^ d[12] ^ d[8] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[3] ^ c[7] ^ c[8] ^ c[9] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[19] ^ c[20] ^ c[23] ^ c[25] ^ c[26] ^ c[31];
    newcrc[29] = d[51] ^ d[50] ^ d[48] ^ d[45] ^ d[44] ^ d[43] ^ d[42] ^ d[40] ^ d[39] ^ d[34] ^ d[33] ^ d[32] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[15] ^ d[14] ^ d[13] ^ d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[8] ^ c[9] ^ c[10] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[20] ^ c[21] ^ c[24] ^ c[26] ^ c[27];
    newcrc[30] = d[52] ^ d[51] ^ d[49] ^ d[46] ^ d[45] ^ d[44] ^ d[43] ^ d[41] ^ d[40] ^ d[35] ^ d[34] ^ d[33] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[23] ^ d[21] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[9] ^ c[10] ^ c[11] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[21] ^ c[22] ^ c[25] ^ c[27] ^ c[28];
    newcrc[31] = d[53] ^ d[52] ^ d[50] ^ d[47] ^ d[46] ^ d[45] ^ d[44] ^ d[42] ^ d[41] ^ d[36] ^ d[35] ^ d[34] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[24] ^ d[22] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[11] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ c[0] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[10] ^ c[11] ^ c[12] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[26] ^ c[28] ^ c[29];
    nextCRC32_C_D56 = newcrc;
end
endfunction

// polynomial: x^32 + x^31 + x^24 + x^22 + x^16 + x^14 + x^8 + x^7 + x^5 + x^3 + x^1 + 1
// data width: 56
// convention: the first serial bit is D[55]
function [31:0] nextCRC32_AIXM_D56;

    input [55:0] Data;
    input [31:0] crc;
    reg [55:0] d;
    reg [31:0] c;
    reg [31:0] newcrc;
begin
    d = Data;
    c = crc;
    
    newcrc[0] = d[55] ^ d[54] ^ d[53] ^ d[50] ^ d[49] ^ d[46] ^ d[39] ^ d[38] ^ d[37] ^ d[36] ^ d[34] ^ d[33] ^ d[31] ^ d[27] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[3] ^ c[7] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[14] ^ c[15] ^ c[22] ^ c[25] ^ c[26] ^ c[29] ^ c[30] ^ c[31];
    newcrc[1] = d[53] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[40] ^ d[36] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[28] ^ d[26] ^ d[25] ^ d[18] ^ d[17] ^ d[9] ^ d[8] ^ d[0] ^ c[1] ^ c[2] ^ c[4] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[16] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[29];
    newcrc[2] = d[54] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[41] ^ d[37] ^ d[36] ^ d[34] ^ d[33] ^ d[32] ^ d[29] ^ d[27] ^ d[26] ^ d[19] ^ d[18] ^ d[10] ^ d[9] ^ d[1] ^ c[2] ^ c[3] ^ c[5] ^ c[8] ^ c[9] ^ c[10] ^ c[12] ^ c[13] ^ c[17] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[30];
    newcrc[3] = d[54] ^ d[51] ^ d[50] ^ d[48] ^ d[46] ^ d[42] ^ d[39] ^ d[36] ^ d[35] ^ d[31] ^ d[30] ^ d[28] ^ d[26] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[9] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[4] ^ c[6] ^ c[7] ^ c[11] ^ c[12] ^ c[15] ^ c[18] ^ c[22] ^ c[24] ^ c[26] ^ c[27] ^ c[30];
    newcrc[4] = d[55] ^ d[52] ^ d[51] ^ d[49] ^ d[47] ^ d[43] ^ d[40] ^ d[37] ^ d[36] ^ d[32] ^ d[31] ^ d[29] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[22] ^ d[19] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[10] ^ d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[0] ^ c[1] ^ c[3] ^ c[5] ^ c[7] ^ c[8] ^ c[12] ^ c[13] ^ c[16] ^ c[19] ^ c[23] ^ c[25] ^ c[27] ^ c[28] ^ c[31];
    newcrc[5] = d[55] ^ d[54] ^ d[52] ^ d[49] ^ d[48] ^ d[46] ^ d[44] ^ d[41] ^ d[39] ^ d[36] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[25] ^ d[22] ^ d[21] ^ d[19] ^ d[17] ^ d[13] ^ d[12] ^ d[10] ^ d[8] ^ d[4] ^ d[1] ^ d[0] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[10] ^ c[12] ^ c[15] ^ c[17] ^ c[20] ^ c[22] ^ c[24] ^ c[25] ^ c[28] ^ c[30] ^ c[31];
    newcrc[6] = d[55] ^ d[53] ^ d[50] ^ d[49] ^ d[47] ^ d[45] ^ d[42] ^ d[40] ^ d[37] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[26] ^ d[23] ^ d[22] ^ d[20] ^ d[18] ^ d[14] ^ d[13] ^ d[11] ^ d[9] ^ d[5] ^ d[2] ^ d[1] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[13] ^ c[16] ^ c[18] ^ c[21] ^ c[23] ^ c[25] ^ c[26] ^ c[29] ^ c[31];
    newcrc[7] = d[55] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[43] ^ d[41] ^ d[39] ^ d[37] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[22] ^ d[20] ^ d[18] ^ d[16] ^ d[13] ^ d[11] ^ d[9] ^ d[7] ^ d[5] ^ d[4] ^ d[1] ^ d[0] ^ c[2] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[13] ^ c[15] ^ c[17] ^ c[19] ^ c[24] ^ c[25] ^ c[27] ^ c[29] ^ c[31];
    newcrc[8] = d[55] ^ d[53] ^ d[52] ^ d[46] ^ d[44] ^ d[42] ^ d[40] ^ d[39] ^ d[37] ^ d[36] ^ d[34] ^ d[32] ^ d[30] ^ d[26] ^ d[24] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[13] ^ d[11] ^ d[9] ^ d[8] ^ d[7] ^ d[4] ^ d[3] ^ d[0] ^ c[0] ^ c[2] ^ c[6] ^ c[8] ^ c[10] ^ c[12] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[20] ^ c[22] ^ c[28] ^ c[29] ^ c[31];
    newcrc[9] = d[54] ^ d[53] ^ d[47] ^ d[45] ^ d[43] ^ d[41] ^ d[40] ^ d[38] ^ d[37] ^ d[35] ^ d[33] ^ d[31] ^ d[27] ^ d[25] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[16] ^ d[14] ^ d[12] ^ d[10] ^ d[9] ^ d[8] ^ d[5] ^ d[4] ^ d[1] ^ c[1] ^ c[3] ^ c[7] ^ c[9] ^ c[11] ^ c[13] ^ c[14] ^ c[16] ^ c[17] ^ c[19] ^ c[21] ^ c[23] ^ c[29] ^ c[30];
    newcrc[10] = d[55] ^ d[54] ^ d[48] ^ d[46] ^ d[44] ^ d[42] ^ d[41] ^ d[39] ^ d[38] ^ d[36] ^ d[34] ^ d[32] ^ d[28] ^ d[26] ^ d[24] ^ d[22] ^ d[20] ^ d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[11] ^ d[10] ^ d[9] ^ d[6] ^ d[5] ^ d[2] ^ c[0] ^ c[2] ^ c[4] ^ c[8] ^ c[10] ^ c[12] ^ c[14] ^ c[15] ^ c[17] ^ c[18] ^ c[20] ^ c[22] ^ c[24] ^ c[30] ^ c[31];
    newcrc[11] = d[55] ^ d[49] ^ d[47] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[39] ^ d[37] ^ d[35] ^ d[33] ^ d[29] ^ d[27] ^ d[25] ^ d[23] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[12] ^ d[11] ^ d[10] ^ d[7] ^ d[6] ^ d[3] ^ c[1] ^ c[3] ^ c[5] ^ c[9] ^ c[11] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[25] ^ c[31];
    newcrc[12] = d[50] ^ d[48] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[40] ^ d[38] ^ d[36] ^ d[34] ^ d[30] ^ d[28] ^ d[26] ^ d[24] ^ d[22] ^ d[21] ^ d[20] ^ d[19] ^ d[17] ^ d[15] ^ d[13] ^ d[12] ^ d[11] ^ d[8] ^ d[7] ^ d[4] ^ c[0] ^ c[2] ^ c[4] ^ c[6] ^ c[10] ^ c[12] ^ c[14] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[26];
    newcrc[13] = d[51] ^ d[49] ^ d[47] ^ d[45] ^ d[44] ^ d[42] ^ d[41] ^ d[39] ^ d[37] ^ d[35] ^ d[31] ^ d[29] ^ d[27] ^ d[25] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[14] ^ d[13] ^ d[12] ^ d[9] ^ d[8] ^ d[5] ^ c[1] ^ c[3] ^ c[5] ^ c[7] ^ c[11] ^ c[13] ^ c[15] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[27];
    newcrc[14] = d[55] ^ d[54] ^ d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[45] ^ d[43] ^ d[42] ^ d[40] ^ d[39] ^ d[37] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[12] ^ d[11] ^ d[7] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[13] ^ c[15] ^ c[16] ^ c[18] ^ c[19] ^ c[21] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
    newcrc[15] = d[55] ^ d[54] ^ d[53] ^ d[50] ^ d[49] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[40] ^ d[38] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[21] ^ d[19] ^ d[18] ^ d[17] ^ d[13] ^ d[12] ^ d[8] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[14] ^ c[16] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[25] ^ c[26] ^ c[29] ^ c[30] ^ c[31];
    newcrc[16] = d[53] ^ d[51] ^ d[49] ^ d[47] ^ d[46] ^ d[45] ^ d[44] ^ d[42] ^ d[41] ^ d[38] ^ d[37] ^ d[35] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[24] ^ d[23] ^ d[21] ^ d[16] ^ d[15] ^ d[12] ^ d[11] ^ d[10] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[11] ^ c[13] ^ c[14] ^ c[17] ^ c[18] ^ c[20] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[27] ^ c[29];
    newcrc[17] = d[54] ^ d[52] ^ d[50] ^ d[48] ^ d[47] ^ d[46] ^ d[45] ^ d[43] ^ d[42] ^ d[39] ^ d[38] ^ d[36] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[22] ^ d[17] ^ d[16] ^ d[13] ^ d[12] ^ d[11] ^ d[2] ^ d[1] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[12] ^ c[14] ^ c[15] ^ c[18] ^ c[19] ^ c[21] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[30];
    newcrc[18] = d[55] ^ d[53] ^ d[51] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[40] ^ d[39] ^ d[37] ^ d[34] ^ d[33] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[23] ^ d[18] ^ d[17] ^ d[14] ^ d[13] ^ d[12] ^ d[3] ^ d[2] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[13] ^ c[15] ^ c[16] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[29] ^ c[31];
    newcrc[19] = d[54] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[41] ^ d[40] ^ d[38] ^ d[35] ^ d[34] ^ d[33] ^ d[32] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[24] ^ d[19] ^ d[18] ^ d[15] ^ d[14] ^ d[13] ^ d[4] ^ d[3] ^ c[0] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[14] ^ c[16] ^ c[17] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[30];
    newcrc[20] = d[55] ^ d[53] ^ d[51] ^ d[50] ^ d[49] ^ d[48] ^ d[46] ^ d[45] ^ d[42] ^ d[41] ^ d[39] ^ d[36] ^ d[35] ^ d[34] ^ d[33] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[25] ^ d[20] ^ d[19] ^ d[16] ^ d[15] ^ d[14] ^ d[5] ^ d[4] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[15] ^ c[17] ^ c[18] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
    newcrc[21] = d[54] ^ d[52] ^ d[51] ^ d[50] ^ d[49] ^ d[47] ^ d[46] ^ d[43] ^ d[42] ^ d[40] ^ d[37] ^ d[36] ^ d[35] ^ d[34] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[26] ^ d[21] ^ d[20] ^ d[17] ^ d[16] ^ d[15] ^ d[6] ^ d[5] ^ c[2] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[16] ^ c[18] ^ c[19] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
    newcrc[22] = d[54] ^ d[52] ^ d[51] ^ d[49] ^ d[48] ^ d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[41] ^ d[39] ^ d[35] ^ d[34] ^ d[32] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[24] ^ d[23] ^ d[20] ^ d[19] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[0] ^ c[2] ^ c[5] ^ c[6] ^ c[7] ^ c[8] ^ c[10] ^ c[11] ^ c[15] ^ c[17] ^ c[19] ^ c[20] ^ c[22] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[30];
    newcrc[23] = d[55] ^ d[53] ^ d[52] ^ d[50] ^ d[49] ^ d[48] ^ d[47] ^ d[45] ^ d[44] ^ d[42] ^ d[40] ^ d[36] ^ d[35] ^ d[33] ^ d[32] ^ d[31] ^ d[30] ^ d[27] ^ d[25] ^ d[24] ^ d[21] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[1] ^ c[3] ^ c[6] ^ c[7] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[16] ^ c[18] ^ c[20] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[31];
    newcrc[24] = d[55] ^ d[51] ^ d[48] ^ d[45] ^ d[43] ^ d[41] ^ d[39] ^ d[38] ^ d[32] ^ d[28] ^ d[27] ^ d[25] ^ d[24] ^ d[23] ^ d[20] ^ d[18] ^ d[17] ^ d[10] ^ d[9] ^ d[1] ^ d[0] ^ c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[8] ^ c[14] ^ c[15] ^ c[17] ^ c[19] ^ c[21] ^ c[24] ^ c[27] ^ c[31];
    newcrc[25] = d[52] ^ d[49] ^ d[46] ^ d[44] ^ d[42] ^ d[40] ^ d[39] ^ d[33] ^ d[29] ^ d[28] ^ d[26] ^ d[25] ^ d[24] ^ d[21] ^ d[19] ^ d[18] ^ d[11] ^ d[10] ^ d[2] ^ d[1] ^ c[0] ^ c[1] ^ c[2] ^ c[4] ^ c[5] ^ c[9] ^ c[15] ^ c[16] ^ c[18] ^ c[20] ^ c[22] ^ c[25] ^ c[28];
    newcrc[26] = d[53] ^ d[50] ^ d[47] ^ d[45] ^ d[43] ^ d[41] ^ d[40] ^ d[34] ^ d[30] ^ d[29] ^ d[27] ^ d[26] ^ d[25] ^ d[22] ^ d[20] ^ d[19] ^ d[12] ^ d[11] ^ d[3] ^ d[2] ^ c[1] ^ c[2] ^ c[3] ^ c[5] ^ c[6] ^ c[10] ^ c[16] ^ c[17] ^ c[19] ^ c[21] ^ c[23] ^ c[26] ^ c[29];
    newcrc[27] = d[54] ^ d[51] ^ d[48] ^ d[46] ^ d[44] ^ d[42] ^ d[41] ^ d[35] ^ d[31] ^ d[30] ^ d[28] ^ d[27] ^ d[26] ^ d[23] ^ d[21] ^ d[20] ^ d[13] ^ d[12] ^ d[4] ^ d[3] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7] ^ c[11] ^ c[17] ^ c[18] ^ c[20] ^ c[22] ^ c[24] ^ c[27] ^ c[30];
    newcrc[28] = d[55] ^ d[52] ^ d[49] ^ d[47] ^ d[45] ^ d[43] ^ d[42] ^ d[36] ^ d[32] ^ d[31] ^ d[29] ^ d[28] ^ d[27] ^ d[24] ^ d[22] ^ d[21] ^ d[14] ^ d[13] ^ d[5] ^ d[4] ^ c[0] ^ c[3] ^ c[4] ^ c[5] ^ c[7] ^ c[8] ^ c[12] ^ c[18] ^ c[19] ^ c[21] ^ c[23] ^ c[25] ^ c[28] ^ c[31];
    newcrc[29] = d[53] ^ d[50] ^ d[48] ^ d[46] ^ d[44] ^ d[43] ^ d[37] ^ d[33] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[25] ^ d[23] ^ d[22] ^ d[15] ^ d[14] ^ d[6] ^ d[5] ^ c[1] ^ c[4] ^ c[5] ^ c[6] ^ c[8] ^ c[9] ^ c[13] ^ c[19] ^ c[20] ^ c[22] ^ c[24] ^ c[26] ^ c[29];
    newcrc[30] = d[54] ^ d[51] ^ d[49] ^ d[47] ^ d[45] ^ d[44] ^ d[38] ^ d[34] ^ d[33] ^ d[31] ^ d[30] ^ d[29] ^ d[26] ^ d[24] ^ d[23] ^ d[16] ^ d[15] ^ d[7] ^ d[6] ^ c[0] ^ c[2] ^ c[5] ^ c[6] ^ c[7] ^ c[9] ^ c[10] ^ c[14] ^ c[20] ^ c[21] ^ c[23] ^ c[25] ^ c[27] ^ c[30];
    newcrc[31] = d[54] ^ d[53] ^ d[52] ^ d[49] ^ d[48] ^ d[45] ^ d[38] ^ d[37] ^ d[36] ^ d[35] ^ d[33] ^ d[32] ^ d[30] ^ d[26] ^ d[25] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[19] ^ d[18] ^ d[17] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[11] ^ d[10] ^ d[9] ^ d[8] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[2] ^ c[6] ^ c[8] ^ c[9] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[21] ^ c[24] ^ c[25] ^ c[28] ^ c[29] ^ c[30];
    nextCRC32_AIXM_D56 = newcrc;
end
endfunction

endpackage
