import bokeh.plotting as plt
from bokeh.io import export_svg
from selenium import webdriver
import numpy as np

driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')

####################################################################
# flooded.svg

x = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1250, 1500]

y = []
y.append([0, 0, 0, 0, 1, 2, 8, 24, 44, 80, 250, 470])
y.append([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2])
y.append([0, 0, 0, 0, 2, 3, 10, 16, 18, 31, 89, 167])
y.append([0, 0, 1, 2, 3, 8, 11, 16, 20, 34, 52, 93])
y.append([0, 1, 3, 9, 16, 26, 37, 53, 70, 110, 165, 274])
y.append([0, 2, 2, 10, 14, 0, 0, 0, 0, 178, 0, 536])

legend = ['A - 4096x2', 'B - 4096x3', 'C - 512x4', 'D - 2048x2', 'E - 2048x2', 'F - 512x2']
color = ['black', 'blue', 'plum', 'cyan', 'lightgreen', 'darkviolet']

p = plt.figure(width=2400, height=800)

for i, flooded in enumerate(y):
    p.line(x, flooded, color=color[i], legend_label=legend[i], line_width=8)
    p.circle(x, flooded, color=color[i], size=32)

p.xaxis.axis_label = 'network addresses'
p.yaxis.axis_label = 'flooded addresses'

p.xaxis.axis_label_text_font = 'garamond'
p.xaxis.axis_label_text_font_style = 'normal'
p.xaxis.axis_label_text_font_size = '32pt'

p.xaxis.major_label_text_font = 'garamond'
p.xaxis.major_label_text_font_size = '28pt'

p.yaxis.axis_label_text_font = 'garamond'
p.yaxis.axis_label_text_font_style = 'normal'
p.yaxis.axis_label_text_font_size = '32pt'

p.yaxis.major_label_text_font = 'garamond'
p.yaxis.major_label_text_font_size = '28pt'

p.legend.label_text_font = 'garamond'
p.legend.label_text_font_size = '32pt'
p.legend.location = "top_left"

plt.output_backend = 'svg'
export_svg(p, filename='../tex/mastersthesis/pic/flooded.svg', webdriver=driver)

####################################################################
# birthday-problem.svg

m = 2 ** 16  # number of possible hash values
nodes = np.linspace(0, 1000, 1001)  # range of maximum number of nodes in network
y = [1 - np.e ** ((-x*(x-1))/(2*m)) for x in nodes]  # probability of collision for m-bit hash function

p = plt.figure(width=2400, height=800)
p.line(nodes, y, line_width=8)

p.xaxis.axis_label = 'network addresses'
p.yaxis.axis_label = 'probability of collision'

p.xaxis.axis_label_text_font = 'garamond'
p.xaxis.axis_label_text_font_style = 'normal'
p.xaxis.axis_label_text_font_size = '32pt'
p.xaxis[0].formatter.use_scientific = False

p.xaxis.major_label_text_font = 'garamond'
p.xaxis.major_label_text_font_size = '28pt'

p.yaxis.axis_label_text_font = 'garamond'
p.yaxis.axis_label_text_font_style = 'normal'
p.yaxis.axis_label_text_font_size = '32pt'

p.yaxis.major_label_text_font = 'garamond'
p.yaxis.major_label_text_font_size = '28pt'

plt.output_backend = 'svg'
export_svg(p, filename='../tex/mastersthesis/pic/birthday-problem.svg', webdriver=driver)

####################################################################
# bloom-filter-nodes.png

plt.output_file('bloom-filter-nodes.html')

fp = [0.1, 0.01, 0.001]  # Bloom filter false-positive probabilities [%]
legend = ['P = 0.1%', 'P = 0.01%', 'P = 0.001%']
nodes = np.linspace(100, 10000, 100)  # range of maximum number of nodes in network

p = plt.figure()

for i, f in enumerate(fp):
    bf_bytes = 1.44 * np.log2(100/f) / 8 / 1000 * nodes
    p.line(nodes, bf_bytes, legend_label=legend[i])

p.xaxis.axis_label = 'maximum number of nodes in network'
p.yaxis.axis_label = 'number of required Bloom filter size [KB]'

p.xaxis.axis_label_text_font = 'garamond'
p.xaxis.axis_label_text_font_style = 'normal'
p.yaxis.axis_label_text_font = 'garamond'
p.yaxis.axis_label_text_font_style = 'normal'
p.legend.label_text_font = 'garamond'

#plt.show(p)

nodes = 1000
for f in fp:
    bf = 1.44 * np.log2(100/f) * nodes / 8
    k = (bf * 8/nodes) * np.log(2)
