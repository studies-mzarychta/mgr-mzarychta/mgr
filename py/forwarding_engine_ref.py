from binascii import hexlify
from crccheck.crc import *
from math import log2
from random import getrandbits


class ForwardingEngineRequest:

    def __init__(self, pid=0, id=0, smac='0', dmac='0', vid=0, has_vid=0, prio=0, has_prio=0):
        self.pid = pid
        self.id = id
        self.smac = smac
        self.dmac = dmac
        self.vid = vid
        self.has_vid = has_vid
        self.prio = prio
        self.has_prio = has_prio

    def hexdump(self):

        req = "{0:08b}".format(self.id) + \
              "{0:048b}".format(int(self.smac, 16)) + \
              "{0:048b}".format(int(self.dmac, 16)) + \
              "{0:012b}".format(self.vid) + \
              "{0:01b}".format(self.has_vid) + \
              "{0:03b}".format(self.prio) + \
              "{0:01b}".format(self.has_prio) + \
              "{0:07b}".format(0)

        return "{0:032X}".format(int(req, 2))


class ForwardingEngineResponse:

    def __init__(self, pid=0, id=0, port_mask='0', prio=0, drop=0):
        self.pid = pid
        self.id = id
        self.port_mask = port_mask
        self.prio = prio
        self.drop = drop
        self.hp = 0

    def hexdump(self):

        resp = "{0:08b}".format(self.id) + \
               "{0:024b}".format(int(self.port_mask, 2)) + \
               "{0:03b}".format(self.prio) + \
               "{0:01b}".format(self.drop) + \
               "{0:01b}".format(self.hp) + \
               "{0:03b}".format(0)

        return "{0:010X}".format(int(resp, 2))


class VtabEntry:

    def __init__(self, vid=0, drop=0, prio_override=0, prio=0, has_prio=0, fid=0, port_mask='0'):
        self.vid = vid
        self.drop = drop
        self.prio_override = prio_override
        self.prio = prio
        self.has_prio = has_prio
        self.fid = fid
        self.port_mask = port_mask

    def hexdump(self):

        entry = "{0:01b}".format(self.drop) + \
                "{0:01b}".format(self.prio_override) + \
                "{0:03b}".format(self.prio) + \
                "{0:01b}".format(self.has_prio) + \
                "{0:08b}".format(self.fid) + \
                "{0:024b}".format(int(self.port_mask, 2)) + \
                "{0:02b}".format(0)

        return "{0:010X}".format(int(entry, 2))

    def parse(self, data):
        self.drop = int(bin(int.from_bytes(data, byteorder="big") >> 39 & 0b1), 2)
        self.prio_override = int(bin(int.from_bytes(data, byteorder="big") >> 38 & 0b1), 2)
        self.prio = int(bin(int.from_bytes(data, byteorder="big") >> 35 & 0b111), 2)
        self.has_prio = int(bin(int.from_bytes(data, byteorder="big") >> 34 & 0b1), 2)
        self.fid = int(bin(int.from_bytes(data, byteorder="big") >> 26 & 0b11111111), 2)
        self.port_mask = bin(int.from_bytes(data, byteorder="big") >> 2 & 0b111111111111111111111111)[2:]


class HtabEntry:

    def __init__(self, fid=0, mac='0', ptr=0):
        self.fid = fid
        self.mac = mac
        self.ptr = ptr

    def hexdump(self):

        entry = "{0:08b}".format(self.fid) + \
                "{0:048b}".format(int(self.mac, 16)) + \
                "{0:014b}".format(self.ptr) + \
                "{0:02b}".format(0)

        return "{0:018X}".format(int(entry, 2))

    def parse(self, data):
        self.fid = int(bin(int.from_bytes(data, byteorder="big") >> 64 & 0b11111111), 2)
        self.mac = "{0:012X}".format(int.from_bytes(data, byteorder="big") >> 16 & 0b111111111111111111111111111111111111111111111111)
        self.ptr = int(bin(int.from_bytes(data, byteorder="big") >> 2 & 0b11111111111111), 2)


class MtabEntry:

    def __init__(self, fid=0, mac='0', port_mask_src='0', port_mask_dst='0', drop_when_src=0, drop_when_dst=0, drop_unmatched_src=0, \
                 has_prio_src=0, prio_override_src=0, prio_src=0, has_prio_dst=0, prio_override_dst=0, prio_dst=0, is_link_local=0):
        self.fid = fid
        self.mac = mac
        self.port_mask_src = port_mask_src
        self.port_mask_dst = port_mask_dst
        self.drop_when_src = drop_when_src
        self.drop_when_dst = drop_when_dst
        self.drop_unmatched_src = drop_unmatched_src
        self.has_prio_src = has_prio_src
        self.prio_override_src = prio_override_src
        self.prio_src = prio_src
        self.has_prio_dst = has_prio_dst
        self.prio_override_dst = prio_override_dst
        self.prio_dst = prio_dst
        self.is_link_local = is_link_local

    def hexdump(self):

        entry = "{0:08b}".format(self.fid) + \
                "{0:048b}".format(int(self.mac, 16)) + \
                "{0:024b}".format(int(self.port_mask_src, 2)) + \
                "{0:024b}".format(int(self.port_mask_dst, 2)) + \
                "{0:01b}".format(self.drop_when_src) + \
                "{0:01b}".format(self.drop_when_dst) + \
                "{0:01b}".format(self.drop_unmatched_src) + \
                "{0:01b}".format(self.has_prio_src) + \
                "{0:01b}".format(self.prio_override_src) + \
                "{0:03b}".format(self.prio_src) + \
                "{0:01b}".format(self.has_prio_dst) + \
                "{0:01b}".format(self.prio_override_dst) + \
                "{0:03b}".format(self.prio_dst) + \
                "{0:01b}".format(self.is_link_local) + \
                "{0:02b}".format(0)

        return "{0:030X}".format(int(entry, 2))

    def parse(self, data):
        self.fid = int(bin(int.from_bytes(data, byteorder="big") >> 112 & 0b11111111), 2)
        self.mac = "{0:012X}".format(int(bin(int.from_bytes(data, byteorder="big") >> 64 & 0b111111111111111111111111111111111111111111111111), 2))  #hex(int(bin(int.from_bytes(data, byteorder="big") >> 64 & 0b111111111111111111111111111111111111111111111111), 2))[2:]
        self.port_mask_src = bin(int.from_bytes(data, byteorder="big") >> 40 & 0b111111111111111111111111)[2:]
        self.port_mask_dst = bin(int.from_bytes(data, byteorder="big") >> 16 & 0b111111111111111111111111)[2:]
        self.drop_when_src = int(bin(int.from_bytes(data, byteorder="big") >> 15 & 0b1), 2)
        self.drop_when_dst = int(bin(int.from_bytes(data, byteorder="big") >> 14 & 0b1), 2)
        self.drop_unmatched_src = int(bin(int.from_bytes(data, byteorder="big") >> 13 & 0b1), 2)
        self.has_prio_src = int(bin(int.from_bytes(data, byteorder="big") >> 12 & 0b1), 2)
        self.prio_override_src = int(bin(int.from_bytes(data, byteorder="big") >> 11 & 0b1), 2)
        self.prio_src = int(bin(int.from_bytes(data, byteorder="big") >> 8 & 0b111), 2)
        self.has_prio_dst = int(bin(int.from_bytes(data, byteorder="big") >> 7 & 0b1), 2)
        self.prio_override_dst = int(bin(int.from_bytes(data, byteorder="big") >> 6 & 0b1), 2)
        self.prio_dst = int(bin(int.from_bytes(data, byteorder="big") >> 3 & 0b111), 2)
        self.is_link_local = int(bin(int.from_bytes(data, byteorder="big") >> 2 & 0b1), 2)


class Memory:

    def __init__(self, slots, slot_size):
        self.__slots = slots
        self.__slot_size = slot_size
        self.__mem = [b'\x00' * self.SLOT_SIZE for i in range(self.SLOTS)]
        self.__empty = [True] * self.__slots

    def memdump(self, filename):
        with open(filename, 'w+') as f:
            for slot, mem in enumerate(self.__mem):
                f.write("@{0:04X} ".format(slot) + str(hexlify(mem).decode('ascii')) + "\n")

    def add(self, slot, data):
        if self.__empty[slot]:
            self.__mem[slot] = data
            self.__empty[slot] = False
        else:
            raise Exception('Slot is not empty.')

    def addToEmpty(self, data):
        for slot, empty in enumerate(self.__empty):
            if empty:
                self.add(slot, data)
                return slot

        raise Exception('No empty slots.')

    def empty(self, slot):
        return self.__empty[slot]

    def read(self, slot):
        return self.__mem[slot]

    @property
    def SLOTS(self):
        return self.__slots

    @property
    def SLOT_SIZE(self):
        return self.__slot_size


class HashMemory(Memory):

    def __init__(self, slots, slot_size, hash_func):
        self.__hash_func = hash_func
        super(HashMemory, self).__init__(slots, slot_size)

    def calcSlot(self, selector):
        bits_num = int(log2(self.SLOTS))
        slot = self.__hash_func.calc(bytearray.fromhex(selector))
        slot = bin(slot)[2:].zfill(bits_num)
        slot = slot[-bits_num:]
        return int(slot, 2)


class VlanTable:

    def __init__(self, depth, data_width):
        self.vtab = Memory(slots=depth, slot_size=data_width)

    def add(self, entry):
        slot = entry.vid
        data = bytes.fromhex(entry.hexdump())
        self.vtab.add(slot, data)

    def search(self, vid):
        if not self.vtab.empty(vid):
            entry = VtabEntry()
            entry.parse(self.vtab.read(vid))
            return entry
        else:
            return None

class MacTable:

    def __init__(self, mtab_depth, mtab_data_width, htab_depth, htab_data_width, subtables, subtables_hash_func):
        self.__subtables_hash_func = subtables_hash_func
        self.__subtables = subtables
        self.htab = [HashMemory(slots=htab_depth, slot_size=htab_data_width, hash_func=self.__subtables_hash_func[i]) for i in range(self.__subtables)]
        self.mtab = Memory(slots=mtab_depth, slot_size=mtab_data_width)

    def calcSelector(self, fid, mac):
        selector = "{0:08b}".format(fid) + "{0:048b}".format(int(mac, 16))
        selector = "{0:014X}".format(int(selector, 2))
        return selector

    def calcMacForSameHash(self, fid, mac, subtable_id):
        selector = self.calcSelector(fid, mac)
        slot = self.htab[subtable_id].calcSlot(selector)

        for i in range(100000):
            calcmac = hex(getrandbits(48))[2:]
            calcselector = self.calcSelector(fid, calcmac)
            calcslot = self.htab[subtable_id].calcSlot(calcselector)
            if slot == calcslot:
                return calcmac

        raise Exception('MAC address could not be calculated successfully.')


    def add(self, entry):
        selector = self.calcSelector(entry.fid, entry.mac)
        data = bytes.fromhex(entry.hexdump())

        for subtable in self.htab:
            slot = subtable.calcSlot(selector)
            if subtable.empty(slot):
                ptr = self.mtab.addToEmpty(data)
                entry = HtabEntry(fid=entry.fid, mac=entry.mac, ptr=ptr)
                data = bytes.fromhex(entry.hexdump())
                subtable.add(slot, data)
                return

        raise Exception('No space left in any subtable.')

    def search(self, fid, mac):
        selector = self.calcSelector(fid, mac)
        for subtable in self.htab:
            slot = subtable.calcSlot(selector)

            if not subtable.empty(slot):

                htab_entry = HtabEntry()
                htab_entry.parse(subtable.read(slot))

                if htab_entry.fid == fid and htab_entry.mac == mac:
                    mtab_entry = MtabEntry()
                    mtab_entry.parse(self.mtab.read(htab_entry.ptr))
                    return mtab_entry

        return None


class ForwardingEngine:

    PORTS_NUM = 24
    PORTS_NUM_PER_ENGINE = 24

    VTAB_DEPTH = 4096
    VTAB_DATA_WIDTH = 5
    HTAB_DEPTH = 4096
    HTAB_DATA_WIDTH = 9
    HTAB_SUBTABLES = 5
    HTAB_SUBTABLES_CRC = [Crc16Ccitt, Crc16Cdma2000, Crc16Arc, Crc32, Crc32C]
    MTAB_DEPTH = 10240
    MTAB_DATA_WIDTH = 15

    def __init__(self, en=1, tru_en=0, pass_all=[1]*PORTS_NUM, pass_link_local=[0]*PORTS_NUM, b_unrec=[1]*PORTS_NUM):
        self.en = en
        self.tru_en = tru_en
        self.pass_all = pass_all
        self.pass_link_local = pass_link_local
        self.b_unrec = b_unrec
        self.vlan_table = VlanTable(self.VTAB_DEPTH, self.VTAB_DATA_WIDTH)
        self.mac_table = MacTable(self.MTAB_DEPTH, self.MTAB_DATA_WIDTH, self.HTAB_DEPTH, self.HTAB_DATA_WIDTH, self.HTAB_SUBTABLES, self.HTAB_SUBTABLES_CRC)

    def processRequest(self, req, tru_resp=None):

        # drop frame response
        resp = ForwardingEngineResponse(pid= req.pid, id=req.id, port_mask='0', prio=0, drop=1)

        # initial configuration verification

        if not self.en:
            return resp

        if not self.pass_all[req.pid] and not self.pass_link_local[req.pid]:
            return resp

        # VLAN configuration verification

        vtab_entry = self.vlan_table.search(req.vid)

        if vtab_entry.drop:
            return resp

        # configuration for source MAC address verification

        mtab_src_entry = self.mac_table.search(vtab_entry.fid, req.smac)

        if mtab_src_entry is None:
            pass
        else:
            if mtab_src_entry.drop_when_src:
                return resp

            if (req.pid & int(mtab_src_entry.port_mask_src, 2) == 0) and mtab_src_entry.drop_unmatched_src:
                return resp

        # configuration for destination MAC address verification

        mtab_dst_entry = self.mac_table.search(vtab_entry.fid, req.dmac)

        if mtab_dst_entry is None:
            if not self.b_unrec[req.pid]:
                return resp
        else:

            if mtab_dst_entry.drop_when_dst:
                return resp

            if not self.pass_all[req.pid]:
                if not mtab_dst_entry.is_link_local:
                    return resp

        # topology resolution configuration verification

        if self.tru_en:
            if tru_resp is not None and tru_resp.drop:
                return resp

        # final forwarding decision

        final_port_mask = vtab_entry.port_mask

        if mtab_dst_entry is not None:
            final_port_mask = bin(int(final_port_mask, 2) & int(mtab_dst_entry.port_mask_dst, 2))[2:]

        if self.tru_en:
            final_port_mask = bin(int(final_port_mask, 2) & int(tru_resp.port_mask, 2))[2:]

        if req.has_prio:
            final_prio = req.has_prio
        elif vtab_entry.has_prio:
            final_prio = vtab_entry.prio
        elif mtab_src_entry is not None and mtab_src_entry.has_prio_src:
            final_prio = mtab_src_entry.prio_src
        elif mtab_dst_entry is not None and mtab_dst_entry.has_prio_dst:
            final_prio = mtab_dst_entry.prio_dst
        else:
            final_prio = 0

        # proceed frame response
        resp = ForwardingEngineResponse(pid= req.pid, id=req.id, port_mask=final_port_mask, prio=final_prio, drop=0)

        return resp

