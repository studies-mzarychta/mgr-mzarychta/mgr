# Crc10Atm, Crc10, Crc10I610, Crc10Cdma2000, Crc10Gsm, \
# Crc11Flexray, Crc11, Crc11Umts, \
# Crc12Cdma2000, Crc12Dect, Crc12X, Crc12Gsm, Crc12Umts, Crc123Gpp, \
# Crc14Darc, Crc14Gsm, \
# Crc15Can, Crc15, Crc15Mpt1327, \
# Crc16Arc, Crc16Lha, Crc16Cdma2000, Crc16Cms, \
# Crc16Dds110, Crc16DectR, Crc16DectX, Crc16Dnp, Crc16En13757, Crc16Genibus, Crc16Darc, Crc16Epc, Crc16EpcC1G2, \
# Crc16ICode, Crc16Gsm, Crc16Ibm3740, Crc16Autosar, Crc16CcittFalse, Crc16IbmSdlc, Crc16IsoHdlc, Crc16IsoIec144433B, \
# Crc16X25, Crc16IsoIec144433A, Crc16Kermit, Crc16Ccitt, Crc16CcittTrue, Crc16V41Lsb, CrcCcitt, Crc16Lj1200, \
# Crc16MaximDow, Crc16Maxim, Crc16Mcrf4XX, Crc16Modbus, Crc16Nrsc5, Crc16OpensafetyA, Crc16OpensafetyB, Crc16Profibus, \
# Crc16Iec611582, Crc16Riello, Crc16SpiFujitsu, Crc16AugCcitt, Crc16T10Dif, Crc16Teledisk, Crc16Tms37157, Crc16Umts, \
# Crc16Buypass, Crc16Verifone, Crc16Usb, Crc16Xmodem, Crc16Acorn, Crc16Lte, Crc16V41Msb, \
# Crc24Ble, Crc24FlexrayA, Crc24FlexrayB, Crc24Interlaken, Crc24LteA, Crc24LteB, Crc24OpenPgp, Crc24, Crc24Os9, \
# Crc32Aixm, Crc32Q, Crc32Autosar, Crc32Base91D, Crc32D, Crc32Bzip2, Crc32Aal5, Crc32DectB, Crc32CdRomEdc, \
# Crc32Cksum, Crc32Posix, Crc32Iscsi, Crc32Base91C, Crc32Castagnoli, Crc32Interlaken, Crc32C, Crc32IsoHdlc, Crc32, \
# Crc32Adccp, Crc32V42, Crc32Xz, Crc32Jamcrc, Crc32Mpeg2, Crc32Xfer

import bokeh.plotting as plt
import numpy as np
import scipy.stats as ss
from bokeh.models import Range1d
from bokeh.io import export_svg
from crccheck.crc import *
from random import randint
from selenium import webdriver

############################################################
# generate set of unique selectors (FID + MAC)
def genSelectors(selectors_num):

    sels = []
    i = 0
    while i < selectors_num:
        mac = randint(0, 0xFFFFFFFFFFFF)
        mac = '{:012x}'.format(mac)
        fid = randint(0, 0xFF)
        fid = '{:02x}'.format(fid)
        sel = bytearray.fromhex(fid)+bytearray.fromhex(mac)
        if sel not in sels:
            sels.append(sel)
            i += 1

    return sels


############################################################
# calculate a hash for given selectors and CRC code
def calculateHash(selectors, crc, buckets_num):
    bits_num = int(np.log2(buckets_num))
    h = []
    for sel in selectors:
        res = crc.calc(sel)
        res = bin(res)[2:].zfill(bits_num)
        res = res[-bits_num:]
        h.append(int(res, 2))
    return h


############################################################
# hash functions uniformity test
def uniformityTest():

    iterations_num = 1
    buckets_num = 1024
    selectors_num = 100000

    hash_functions = [Crc10, Crc11, Crc16Ccitt, Crc16Cdma2000, Crc16DectX, Crc16Arc, Crc32, Crc32C, Crc32Q]
    hash_functions_names = ['CRC-10', 'CRC-11', 'CRC-16-CCITT', 'CRC-16-CDMA2000', 'CRC-16-DECT', 'CRC-16-IBM',
                            'CRC-32', 'CRC-32C', 'CRC-32-AIXM']

    driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')

    for it in range(iterations_num):
        selectors = genSelectors(selectors_num)
        std_map = {}
        min_map = {}
        max_map = {}
        x2_map = {}
        p_map = {}

        for i, func in enumerate(hash_functions):

            # generate histogram and chi-squared test
            h = calculateHash(selectors, func, buckets_num)
            hist, edges = np.histogram(h, bins=list(range(buckets_num)))
            x2, p_value = ss.chisquare(hist)

            # generate figure
            p = plt.figure(width=3200, height=800)

            p.quad(top=hist, bottom=0, left=edges[:-1], right=edges[1:],
                   fill_color="navy", line_color="navy", alpha=1, fill_alpha=1, line_width=1.0)

            p.title.text = '{}'.format(hash_functions_names[i])
            p.title.align = 'center'
            p.title.text_font = 'garamond'
            p.title.text_font_style = 'normal'
            p.title.text_font_size = '32pt'

            p.xaxis.axis_label = 'buckets'
            p.yaxis.axis_label = 'count'

            p.xaxis.axis_label_text_font = 'garamond'
            p.xaxis.axis_label_text_font_style = 'normal'
            p.xaxis.axis_label_text_font_size = '32pt'
            p.xaxis[0].formatter.use_scientific = False

            p.xaxis.major_label_text_font = 'garamond'
            p.xaxis.major_label_text_font_size = '28pt'

            p.yaxis.axis_label_text_font = 'garamond'
            p.yaxis.axis_label_text_font_style = 'normal'
            p.yaxis.axis_label_text_font_size = '32pt'

            p.yaxis.major_label_text_font = 'garamond'
            p.yaxis.major_label_text_font_size = '28pt'

            std_map[hash_functions_names[i]] = round(hist.std(), 2)
            min_map[hash_functions_names[i]] = min(hist)
            max_map[hash_functions_names[i]] = max(hist)
            x2_map[hash_functions_names[i]] = round(x2, 2)
            p_map[hash_functions_names[i]] = round(p_value, 2)

            plt.output_backend = 'svg'
            fname = '../tex/mastersthesis/pic/uniformity-{}.svg'.format(hash_functions_names[i])
            export_svg(p, filename=fname, webdriver=driver)

        print(sorted(std_map.items()))
        print(sorted(min_map.items()))
        print(sorted(max_map.items()))
        print(sorted(x2_map.items()))
        print(sorted(p_map.items()))


############################################################
# sum hash table collisions
def hashTableCollisions(hash_functions, selectors, sub_tables_num, buckets_num, slots_per_bucket_num):
    hash_table = [[[] for _ in range(buckets_num)] for _ in range(sub_tables_num)]

    collisions = 0
    for sel in selectors:
        put = False
        for i, func in enumerate(range(sub_tables_num)):
            hash = calculateHash([sel], hash_functions[i], buckets_num)[0]
            if len(hash_table[i][hash]) < slots_per_bucket_num:
                hash_table[i][hash].append(1)
                put = True
                break

        if not put:
            collisions += 1

    return collisions


############################################################
# hash sub-tables structure test
def hashTableStructureTest():

    iterations_num = 100
    selectors_num = [4000, 5000, 6000, 7000, 8000, 9000, 10000]

    hash_functions = [Crc16Ccitt, Crc16Cdma2000, Crc16DectX, Crc16Arc, Crc32, Crc32C, Crc32Q,
                      Crc32Mpeg2, Crc16OpensafetyA, Crc16OpensafetyB]

    # (sub-tables number, buckets number, slots per bucket number)
    sets = [(10, 1024, 1),
            (1, 1024, 10),
            (10, 2048, 1),
            (8, 2048, 1),
            (5, 2048, 2),
            (4, 2048, 2),
            (2, 2048, 4),
            (1, 2048, 8),
            (4, 4096, 1),
            (5, 4096, 1)]

    avg_results = [[[] for _ in range(len(selectors_num))] for _ in range(len(sets))]
    p_results = [[[] for _ in range(len(selectors_num))] for _ in range(len(sets))]

    for s, sel in enumerate(selectors_num):

        print('number of selectors: {}'.format(sel))

        results = [[] for _ in range(len(sets))]

        for it in range(iterations_num):
            # print("iteration: " + str(it) + " out of " + str(iterations_num))
            selectors = genSelectors(sel)

            for i, set in enumerate(sets):
                collisions = hashTableCollisions(hash_functions, selectors, set[0], set[1], set[2])
                results[i].append(collisions)

        for i, set in enumerate(sets):
            p = (len([x for x in results[i] if x != 0]) / iterations_num) * 100
            avg = np.mean(results[i])
            print('sub-tables: {}, buckets: {}, slots per bucket: {}, avg collisions: {}, '
                  'probability of collisions: {}%'.format(set[0], set[1], set[2], avg, p))
            # print('collisions list: {}'.format(results[i]))

            p_results[i][s].append(p)
            avg_results[i][s].append(avg)

    # generate figures
    driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')

    color = ['black', 'blue', 'plum', 'cyan', 'lightgreen', 'darkviolet', 'green', 'red', 'pink', 'orange']
    p = plt.figure(width=2400, height=1200)
    r = plt.figure(width=2400, height=1200)

    p.y_range = Range1d(-10, 110)
    r.y_range = Range1d(-10, 110)

    for i, set in enumerate(sets):
        p.circle(selectors_num, avg_results[i], size=30, color=color[i])
        p.line(selectors_num, avg_results[i], line_width=6.0, color=color[i], legend_label="d={}, m={}, s={}".format(set[0], set[1], set[2]))
        r.circle(selectors_num, p_results[i], size=30, color=color[i])
        r.line(selectors_num, p_results[i], line_width=6.0, color=color[i], legend_label="d={}, m={}, s={}".format(set[0], set[1], set[2]))

    p.xaxis.axis_label = 'number of entries stored in a MAC table'
    p.yaxis.axis_label = 'average number of unresolved collisions'

    p.legend.location = 'top_left'
    p.legend.label_text_font = 'garamond'
    p.legend.label_text_font_style = 'normal'
    p.legend.label_text_font_size = '32pt'

    p.xaxis.axis_label_text_font = 'garamond'
    p.xaxis.axis_label_text_font_style = 'normal'
    p.xaxis.axis_label_text_font_size = '32pt'
    p.xaxis[0].formatter.use_scientific = False

    p.xaxis.major_label_text_font = 'garamond'
    p.xaxis.major_label_text_font_size = '28pt'

    p.yaxis.axis_label_text_font = 'garamond'
    p.yaxis.axis_label_text_font_style = 'normal'
    p.yaxis.axis_label_text_font_size = '32pt'

    p.yaxis.major_label_text_font = 'garamond'
    p.yaxis.major_label_text_font_size = '28pt'

    r.xaxis.axis_label = 'number of entries stored in a MAC table'
    r.yaxis.axis_label = 'probability of unresolved collision'

    r.legend.location = 'top_left'
    r.legend.label_text_font = 'garamond'
    r.legend.label_text_font_style = 'normal'
    r.legend.label_text_font_size = '32pt'

    r.xaxis.axis_label_text_font = 'garamond'
    r.xaxis.axis_label_text_font_style = 'normal'
    r.xaxis.axis_label_text_font_size = '32pt'
    r.xaxis[0].formatter.use_scientific = False

    r.xaxis.major_label_text_font = 'garamond'
    r.xaxis.major_label_text_font_size = '28pt'

    r.yaxis.axis_label_text_font = 'garamond'
    r.yaxis.axis_label_text_font_style = 'normal'
    r.yaxis.axis_label_text_font_size = '32pt'

    r.yaxis.major_label_text_font = 'garamond'
    r.yaxis.major_label_text_font_size = '28pt'

    plt.output_backend = 'svg'

    export_svg(p, filename='../tex/mastersthesis/pic/hash-table-structure-avg.svg', webdriver=driver)
    export_svg(r, filename='../tex/mastersthesis/pic/hash-table-structure-prob.svg', webdriver=driver)


############################################################
if __name__ == "__main__":
    uniformityTest()
    hashTableStructureTest()
