from random import randint
from forwarding_engine_ref import *


def print_stimulus(path, transactions):

    for pid, pid_transactions in enumerate(transactions):
        with open("{}{}.stim".format(path, pid), "w+") as f:
            for req in pid_transactions:
                f.write(req.hexdump() + "\n")
        f.close()


if __name__ == "__main__":

    ######################################################################
    engine = ForwardingEngine(en=1, pass_all=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], \
                             pass_link_local=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])

    ######################################################################
    # memory initialization

    # VLAN table
    engine.vlan_table.add(VtabEntry(vid=0, fid=0, port_mask='111111111111111111111111'))
    engine.vlan_table.add(VtabEntry(vid=4, fid=255, port_mask='000000000000000000010001'))
    engine.vlan_table.add(VtabEntry(vid=4095, fid=255, port_mask='111111111111111111111111'))

    # MAC table
    engine.mac_table.add(MtabEntry(fid=255, mac='010101010101', port_mask_src='111111111111111111111111'))
    engine.mac_table.add(MtabEntry(fid=255, mac='020202020202', port_mask_dst='111000000000000001111110'))
    engine.mac_table.add(MtabEntry(fid=255, mac='030303030303', port_mask_src='111111111111111111111111', drop_when_src=1))
    engine.mac_table.add(MtabEntry(fid=255, mac='040404040404', port_mask_src='111000000000000000000000', drop_unmatched_src=1))
    engine.mac_table.add(MtabEntry(fid=255, mac='050505050505', port_mask_dst='111111111111111111111111', drop_when_dst=1))
    engine.mac_table.add(MtabEntry(fid=255, mac='060606060606', port_mask_dst='111111111111111111111111', is_link_local=1))

    mac = engine.mac_table.calcMacForSameHash(fid=255, mac='010101010101', subtable_id=0)
    engine.mac_table.add(MtabEntry(fid=255, mac=mac, port_mask_src='111111111111111111111111'))

    ######################################################################
    # generating requests
    reqs = [[] for i in range(engine.PORTS_NUM_PER_ENGINE)]

    # pass all parameter not set for a frame source port
    req = ForwardingEngineRequest(pid=11, id=0, vid=4095, has_vid=1, smac='010101010101', dmac='020202020202')
    reqs[req.pid].append(req)

    # pass link local parameter not set for a frame source port
    req = ForwardingEngineRequest(pid=12, id=1, vid=4095, has_vid=1, smac='010101010101', dmac='020202020202')
    reqs[req.pid].append(req)

    # source MAC address not found, learning enabled
    req = ForwardingEngineRequest(pid=0, id=2, vid=4095, has_vid=1, smac='1234567890AB', dmac='020202020202')
    reqs[req.pid].append(req)

    # source MAC address found, drop_when_src flag set
    req = ForwardingEngineRequest(pid=1, id=3, vid=4095, has_vid=1, smac='030303030303', dmac='020202020202')
    reqs[req.pid].append(req)

    # source MAC address found, a frame source port is inconsistent with port_source_mask, drop_unmatched_src flag set
    req = ForwardingEngineRequest(pid=2, id=4, vid=4095, has_vid=1, smac='040404040404', dmac='020202020202')
    reqs[req.pid].append(req)

    # destination MAC address found, drop_when_dst flag set
    req = ForwardingEngineRequest(pid=3, id=5, vid=4095, has_vid=1, smac='010101010101', dmac='050505050505')
    reqs[req.pid].append(req)

    # destination MAC address not found, broadcasting enabled
    req = ForwardingEngineRequest(pid=4, id=6, vid=4095, has_vid=1, smac='010101010101', dmac='1234567890AB')
    reqs[req.pid].append(req)

    req = ForwardingEngineRequest(pid=4, id=7, vid=4095, has_vid=1, smac='010101010101', dmac='000000000000')
    reqs[req.pid].append(req)

    # accepting frames with link-local destination MAC address when pass all parameter
    # is not set and pass link local parameter is set for a frame source port

    req = ForwardingEngineRequest(pid=11, id=8, vid=4095, has_vid=1, smac='010101010101', dmac='060606060606')
    reqs[req.pid].append(req)

    # forwarding engine processing with highest performance (new requests in every clock cycle)
    for i in range(10000):
        req = ForwardingEngineRequest(pid=randint(0, 23), id=i+100, vid=4095, has_vid=1, smac='010101010101', dmac='020202020202')
        reqs[req.pid].append(req)
        req = ForwardingEngineRequest(pid=randint(0, 23), id=i+100, vid=4095, has_vid=1, smac=mac, dmac='020202020202')
        reqs[req.pid].append(req)

    ######################################################################
    # generating responses
    resps = [[] for i in range(engine.PORTS_NUM_PER_ENGINE)]

    for i in range(engine.PORTS_NUM_PER_ENGINE):
        for req in reqs[i]:
            resp = engine.processRequest(req)
            resps[i].append(resp)

    ######################################################################
    # print stimulus
    engine.vlan_table.vtab.memdump("../hdl/forwarding-engine/forwarding-engine.srcs/sources_1/new/mem/vtab.mem")
    engine.mac_table.mtab.memdump("../hdl/forwarding-engine/forwarding-engine.srcs/sources_1/new/mem/mtab.mem")
    for i, htab in enumerate(engine.mac_table.htab):
        htab.memdump("../hdl/forwarding-engine/forwarding-engine.srcs/sources_1/new/mem/htab_{}.mem".format(i))

    print_stimulus("../hdl/forwarding-engine/forwarding-engine.srcs/sources_1/new/stimulus/input/input", reqs)
    print_stimulus("../hdl/forwarding-engine/forwarding-engine.srcs/sources_1/new/stimulus/output/output", resps)




